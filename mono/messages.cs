﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

//namespace LakerfieldBot
//{
//  class messages
//  {
//  }
//}


class MsgWrapper
{
  public string msgType;
  public Object data;

  public MsgWrapper(string msgType, Object data)
  {
    this.msgType = msgType;
    this.data = data;
  }
}


abstract class SendMsg
{
  public string ToJson()
  {
    return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
  }
  protected virtual Object MsgData()
  {
    return this;
  }

  protected abstract string MsgType();
}

class Join : SendMsg
{
  public string name;
  public string key;
  public string color;

  public Join(string name, string key)
  {
    this.name = name;
    this.key = key;
    this.color = "red";
  }

  protected override string MsgType()
  {
    return "join";
  }
}

class Ping : SendMsg
{
  protected override string MsgType()
  {
    return "ping";
  }
}

class Throttle : SendMsg
{
  public double value;

  public Throttle(double value)
  {
    this.value = value;
  }

  protected override Object MsgData()
  {
    return this.value;
  }

  protected override string MsgType()
  {
    return "throttle";
  }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Bot.Ai;
using Bot.Connection;
using Bot.Messages;

namespace Bot
{
  public class Engine
  {

    public async static Task StartBot<T>(string host, int port, string botName, string botKey, JoinType joinType = JoinType.Default, RaceData raceData = null, bool enableLogging = true, Func<IConnection, BaseMessage, T> createBot = null) where T : IBotBase
    {
      using (var client = new TcpClient(host, port))
      {
        var stream = client.GetStream();
        var reader = new StreamReader(stream);
        var writer = new StreamWriter(stream);
        writer.AutoFlush = true;

        var connection = new NetworkConnection(reader, writer) as IConnection;
        if (enableLogging)
          connection = new ConnectionLogger(connection, string.Format(@"HwoLog-{0:MM-dd-HHmm-ss}.txt", DateTime.Now));

        var botId = new JoinBotId()
        {
          Name = botName,
          Key = botKey
        };
        var joinMessage = GetJoinMessage<T>(joinType, botId, raceData);

        var bot = createBot == null 
          ? (T)Activator.CreateInstance(typeof(T), connection, joinMessage)
          : createBot(connection, joinMessage);
        
        Console.WriteLine(@"{0} named {1} connecting to {2}:{3}",
            bot.GetType().Name,
            botName,
            host,
            port);

        await bot.Play();

        if (enableLogging)
          ((ConnectionLogger)connection).Save();
      }
    }

    private static BaseMessage GetJoinMessage<T>(JoinType joinType, JoinBotId botId, RaceData raceData) where T : IBotBase
    {
      switch (joinType)
      {
        default:
        case JoinType.Default:
          return new JoinMessage(botId);

        case JoinType.CreateRace:
          return new CreateRaceMessage(botId, raceData);

        case JoinType.JoinRace:
          return new JoinRaceMessage(botId, raceData);
      }
    }

  }
}

﻿using System;
using System.Linq;
using Bot.Messages;
using Bot.Models;

namespace Bot.Parser
{
  public class GameInitParser
  {
    private GameInitMessage _message;

    public GameInitParser(GameInitMessage message)
    {
      _message = message;
    }

    public Race Parse()
    {
      var race = new Race();

      race.Laps = _message.Data.Race.RaceSession.Laps;
      race.MaxLapTimeMs = _message.Data.Race.RaceSession.MaxLapTimeMs;
      race.QuickRace = _message.Data.Race.RaceSession.QuickRace;

      race.Lanes = _message.Data.Race.Track.Lanes.Select(GetLane).OrderBy(l => l.Index).ToArray();

      race.Track.Id = _message.Data.Race.Track.Id;
      race.Track.Pieces = _message.Data.Race.Track.Pieces.Select((p, i) => GetPiece(race.Lanes, p, i)).ToArray();

      race.Cars = _message.Data.Race.Cars.Select(GetCar).ToArray();

      //TODO: race.StartingPoint
      race.Track.StartX = _message.Data.Race.Track.StartingPoint.Position.X;
      race.Track.StartY = _message.Data.Race.Track.StartingPoint.Position.Y;
      race.Track.StartAngle = _message.Data.Race.Track.StartingPoint.Angle;

      var angle = race.Track.Pieces.Sum(p => p.Angle);
      int rest;
      Math.DivRem(Convert.ToInt32(angle), 360, out rest);
      if (rest != 0)
        Console.WriteLine("Warning: Track possibly not valid ({0} degree)", rest);

      return race;
    }

    private Car GetCar(GameInitMessage.GameInitCar input)
    {
      var result = new Car();

      result.Name = input.Id.Name;
      result.Color = input.Id.Color;
      result.Length = input.Dimensions.Length;
      result.Width = input.Dimensions.Width;
      result.Offset = input.Dimensions.GuideFlagPosition;

      return result;
    }

    private Lane GetLane(GameInitMessage.GameInitLane input)
    {
      var result = new Lane();

      result.Index = input.Index;
      result.DistanceFromCenter = input.DistanceFromCenter;

      return result;
    }

    private TrackPiece GetPiece(Lane[] lanes, GameInitMessage.GameInitPiece input, int index)
    {
      var piece = new TrackPiece();

      piece.Index = index;
      piece.Angle = input.Angle;
      piece.Radius = input.Radius;
      piece.Length = piece.IsCurve()
        ? GetOmtrek(piece.Radius, piece.AngleAbs)
        : input.Length;

      piece.Switch = input.Switch;

      piece.Lanes = lanes.Select(l =>
      {
        var lane = new TrackPieceLane();

        lane.Index = l.Index;

        lane.Angle = piece.Angle;
        switch (lane.Way)
        {
          case Way.Left:
            lane.Radius = piece.Radius + l.DistanceFromCenter;
            break;

          case Way.Straight:
            lane.Radius = piece.Radius;
            break;

          case Way.Right:
            lane.Radius = piece.Radius - l.DistanceFromCenter;
            break;
        }

        lane.Length = lane.IsCurve()
          ? GetOmtrek(lane.Radius, lane.AngleAbs)
          : piece.Length;

        // For wpf trackview
        lane.DistanceFromCenter = l.DistanceFromCenter;

        return lane;
      }).ToArray();
      
      return piece;
    }
    
    public double GetOmtrek(double radius, double hoekInGraden = 360)
    {
      return 2 * radius * Math.PI * hoekInGraden / 360;
    }


    //public static bool Exists(dynamic settings, string name)
    //{
    //  return settings.GetType().GetProperty(name) != null;
    //}
  }
}

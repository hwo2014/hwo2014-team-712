﻿using Bot.Models;

namespace Bot.Parser
{
  public static class Extensions
  {

    public static bool IsCurve(this ITrackPieceLane lane)
    {
      return lane.Radius != 0 
          && lane.Angle != 0;
    }



  }
}

﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;

namespace Bot.Legacy
{
  public class Bot
  {
    public static void Start(string[] args)
    {
      string host = args[0];
      int port = int.Parse(args[1]);
      string botName = args[2];
      string botKey = args[3];

      Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

      using (TcpClient client = new TcpClient(host, port))
      {
        NetworkStream stream = client.GetStream();
        StreamReader reader = new StreamReader(stream);
        StreamWriter writer = new StreamWriter(stream);
        writer.AutoFlush = true;

        new Bot(reader, writer, new Join(botName, botKey));
      }

      Console.Beep();
    }

    private StreamWriter writer;
    //private StringBuilder log;

    private Bot(StreamReader reader, StreamWriter writer, Join join)
    {
      //this.log = new StringBuilder();
      this.writer = writer;
      string line;

      send(join);

      while ((line = reader.ReadLine()) != null)
      {
        //log.AppendLine(string.Format(@">> {0}", line));

        //dynamic message = JsonConvert.DeserializeObject(line);
        MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
        switch (msg.msgType)
        {
          case "carPositions":
            send(new Throttle(0.65));
            break;
          case "join":
            Console.WriteLine("Joined");
            send(new Ping());
            break;
          case "gameInit":
            Console.WriteLine("Race init");
            send(new Ping());
            break;
          case "gameEnd":
            Console.WriteLine("Race ended");
            send(new Ping());
            break;
          case "gameStart":
            Console.WriteLine("Race starts");
            send(new Ping());
            break;
          default:
            Console.WriteLine(msg.msgType);
            send(new Ping());
            break;
        }
      }

      //File.WriteAllText(string.Format(@"HwoLog-{0:MM-dd-HHmm-ss}.txt", DateTime.Now), log.ToString());
    }

    private void send(SendMsg msg)
    {
      //Console.WriteLine(msg.ToString());
      //log.AppendLine(string.Format(@"<< {0}", msg.ToJson()));
      writer.WriteLine(msg.ToJson());
    }
  }



}
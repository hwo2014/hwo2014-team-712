﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot
{
  public static class Constants
  {

    public static Way GetWayFromAngle(double angle)
    {
      return (Way)Math.Sign(angle);
    }


  }

  public enum JoinType
  {
    Default,
    CreateRace,
    JoinRace
  }

  public enum Way
  {
    Left = -1,
    Straight = 0,
    Right = 1
  }

  public enum CarState
  {
    Driving,
    Crashed,
    Finished,
    Dnf
  }

  public enum SwitchLane
  {
    Left = -1,
    None = 0,
    Right = 1
  }

  public enum GameTickResponseMessage
  {
    Throttle,
    Switch,
    Turbo,
    Stop
  }

  public enum GameTickResponsePriority
  {
    ForceForMath = -2,

    Override = -1,

    NeededToMakeCorner = 0,

    TurboIfPossible = 50,
    
    SwitchLane = 75,

    Optimal = 100,

    IfNothingElse = 1000

  }

}

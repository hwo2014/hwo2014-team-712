﻿namespace Bot.Models
{
  public class Car
  {

    public string Name { get; set; }
    public string Color { get; set; }

    public double Length { get; set; }
    public double Width { get; set; }
    public double Offset { get; set; }

  }
}

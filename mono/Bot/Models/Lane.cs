﻿namespace Bot.Models
{
  public class Lane
  {
    public double DistanceFromCenter { get; set; }
    public int Index { get; set; }

    // Calculated
    public double TotalLength { get; set; }
  }
}

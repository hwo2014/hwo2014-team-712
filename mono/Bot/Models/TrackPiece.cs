﻿using System;

namespace Bot.Models
{
  public class TrackPiece : ITrackPieceLane
  {
    public int Index { get; set; }

    public double Length { get; set; }
    public double Radius { get; set; }

    private double _angle;
    public double Angle
    {
      get { return _angle; }
      set
      {
        _angle = value;
        Way = Constants.GetWayFromAngle(_angle);
        AngleAbs = Math.Abs(_angle);
      }
    }

    public bool Switch { get; set; }

    public double AngleAbs { get; set; }
    public Way Way { get; set; }


    // Wpf
    public double GraphX { get; set; }
    public double GraphY { get; set; }
    public double GraphA { get; set; }

    public TrackPieceLane[] Lanes { get; set; }
    public TrackPieceLane this[int startLaneIndex, int endLaneIndex]
    {
      get
      {
        // TODO switches...
        return Lanes[endLaneIndex];
      }
    }

  }
}

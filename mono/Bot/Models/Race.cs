﻿namespace Bot.Models
{
  public class Race
  {
    private Track _track;
    public Track Track
    {
      get
      {
        if (_track == null)
          _track = new Track();
        return _track;
      }
      set { _track = value; }
    }

    public Lane[] Lanes { get; set; }
    public Car[] Cars { get; set; }

    public int Laps { get; set; }

    public double MaxLapTimeMs { get; set; }

    public bool QuickRace { get; set; }


    public Race()
    {
      Lanes = new Lane[0];
    }

  }
}

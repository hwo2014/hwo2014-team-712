﻿namespace Bot.Models
{
  public class Track
  {

    public string Id { get; set; }
    public TrackPiece[] Pieces { get; set; }

    // Wpf only
    public double StartX { get; set; }
    public double StartY { get; set; }
    public double StartAngle { get; set; }


    public Track()
    {
      Pieces = new TrackPiece[0];
    }

  }
}

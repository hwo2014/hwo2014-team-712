﻿using System;

namespace Bot.Models
{
  public class TrackPieceLane : ITrackPieceLane
  {
    public int Index { get; set; }

    public double Length { get; set; }

    private double _angle;
    public double Angle
    {
      get { return _angle; }
      set
      {
        _angle = value;
        Way = Constants.GetWayFromAngle(_angle);
        AngleAbs = Math.Abs(_angle);
      }
    }

    public double Radius { get; set; }

    public double AngleAbs { get; set; }
    public Way Way { get; set; }
    public double DistanceFromCenter { get; set; }
    
    // Calculated
    public double DistanceToNextSwitch { get; set; }


    // TEST
    public double TempEntrySpeed { get; set; }
    public double TempMaxSpeed { get; set; }

  
  }
}

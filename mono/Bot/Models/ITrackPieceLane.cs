﻿namespace Bot.Models
{
  public interface ITrackPieceLane
  {
    double Length { get; set; }
    double Angle { get; set; }
    double Radius { get; set; }
  }
}
﻿using Newtonsoft.Json;

namespace Bot.Messages
{
  public class BaseMessage
  {
    [JsonProperty(PropertyName = "msgType")]
    public string MsgType { get; set; }

    [JsonProperty(PropertyName = "gameTick", DefaultValueHandling = DefaultValueHandling.Ignore)]
    public int GameTick { get; set; }

    public BaseMessage()
    {
    }

    public BaseMessage(string msgType)
    {
      MsgType = msgType;
    }
  }

  public class BaseMessage<T> : BaseMessage
  {
    [JsonProperty(PropertyName = "data")]
    public T Data { get; set; }

    public BaseMessage(string msgType) : base(msgType)
    {
    }
    public BaseMessage()
    {
    }

  }
}

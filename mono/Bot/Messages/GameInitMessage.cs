﻿using System;
using Newtonsoft.Json;

namespace Bot.Messages
{
  public class GameInitMessage : BaseMessage
  {
    //[JsonProperty(PropertyName = "msgType")]
    //public string MsgType { get; set; }

    [JsonProperty(PropertyName = "gameId")]
    public Guid GameId { get; set; }

    [JsonProperty(PropertyName = "data")]
    public GameInitData Data { get; set; }



    public class GameInitData
    {
      [JsonProperty(PropertyName = "race")]
      public GameInitRace Race { get; set; }
    }

    public class GameInitRace
    {
      [JsonProperty(PropertyName = "track")]
      public GameInitTrack Track { get; set; }

      [JsonProperty(PropertyName = "cars")]
      public GameInitCar[] Cars { get; set; }

      [JsonProperty(PropertyName = "raceSession")]
      public GameInitRaceSession RaceSession { get; set; }

    }

    public class GameInitTrack
    {
      [JsonProperty(PropertyName = "id")]
      public string Id { get; set; }

      [JsonProperty(PropertyName = "name")]
      public string Name { get; set; }

      [JsonProperty(PropertyName = "pieces")]
      public GameInitPiece[] Pieces { get; set; }

      [JsonProperty(PropertyName = "lanes")]
      public GameInitLane[] Lanes { get; set; }

      [JsonProperty(PropertyName = "startingPoint")]
      public GameInitStartingPoint StartingPoint { get; set; }
    }

    public class GameInitPiece
    {
      [JsonProperty(PropertyName = "length")]
      public double Length { get; set; }

      [JsonProperty(PropertyName = "switch")]
      public bool Switch { get; set; }

      [JsonProperty(PropertyName = "radius")]
      public double Radius { get; set; }

      [JsonProperty(PropertyName = "angle")]
      public double Angle { get; set; }
    }

    public class GameInitLane
    {
      [JsonProperty(PropertyName = "index")]
      public int Index { get; set; }

      [JsonProperty(PropertyName = "distanceFromCenter")]
      public double DistanceFromCenter { get; set; }
    }

    public class GameInitStartingPoint
    {
      [JsonProperty(PropertyName = "angle")]
      public double Angle { get; set; }

      [JsonProperty(PropertyName = "position")]
      public GameInitStartingPosition Position { get; set; }
    }
    
    public class GameInitStartingPosition
    {
      [JsonProperty(PropertyName = "x")]
      public double X { get; set; }

      [JsonProperty(PropertyName = "y")]
      public double Y { get; set; }
    }

    public class GameInitCar
    {
      [JsonProperty(PropertyName = "id")]
      public GameInitCarId Id { get; set; }

      [JsonProperty(PropertyName = "dimensions")]
      public GameInitDimensions Dimensions { get; set; }
    }

    public class GameInitCarId
    {
      [JsonProperty(PropertyName = "name")]
      public string Name { get; set; }

      [JsonProperty(PropertyName = "color")]
      public string Color { get; set; }
    }

    public class GameInitDimensions
    {
      [JsonProperty(PropertyName = "length")]
      public double Length { get; set; }

      [JsonProperty(PropertyName = "width")]
      public double Width { get; set; }

      [JsonProperty(PropertyName = "guideFlagPosition")]
      public double GuideFlagPosition { get; set; }
    }

    public class GameInitRaceSession
    {
      [JsonProperty(PropertyName = "laps")]
      public int Laps { get; set; }

      [JsonProperty(PropertyName = "maxLapTimeMs")]
      public double MaxLapTimeMs { get; set; }

      [JsonProperty(PropertyName = "quickRace")]
      public bool QuickRace { get; set; }

      [JsonProperty(PropertyName = "durationMs")]
      public bool DurationMs { get; set; }

      // Qualify
      // "raceSession":{"durationMs":20000}

      // Race
      // "raceSession":{"laps":5,"maxLapTimeMs":60000,"quickRace":false}

    }


  }



}

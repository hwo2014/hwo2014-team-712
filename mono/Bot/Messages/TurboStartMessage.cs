﻿namespace Bot.Messages
{
  public class TurboStartMessage : BaseMessage<CarData>, ICarStateMessage
  {
    public TurboStartMessage()
      : base("turboStart")
    {
    }
  }
}

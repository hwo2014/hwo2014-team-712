﻿namespace Bot.Messages
{

  //{"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}

  public class TurboMessage : BaseMessage<string>
  {
    public TurboMessage()
      : base("turbo")
    {
    }
  }
}

﻿using Newtonsoft.Json;

namespace Bot.Messages
{
  public class ThrottleMessage : BaseMessage<double>
  {
    public ThrottleMessage()
      : base("throttle")
    {
    }
  }

}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Bot.Messages
{

  //{"msgType": "switchLane", "data": "Left"}

  public class SwitchLaneMessage : BaseMessage
  {
    //[JsonProperty(PropertyName = "gameTick", DefaultValueHandling = DefaultValueHandling.Ignore)]
    //public int GameTick { get; set; }

    [JsonConverter(typeof(StringEnumConverter))]
    [JsonProperty(PropertyName = "data")]
    public SwitchLane Data { get; set; }

    public SwitchLaneMessage()
      : base("switchLane")
    {
    }
  }
}

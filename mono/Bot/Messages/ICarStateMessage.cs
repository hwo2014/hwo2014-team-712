﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot.Messages
{
  public interface IStateMessage
  {
  }

  public interface ICarStateMessage : IStateMessage
  {
    CarData Data { get; }
  }

}

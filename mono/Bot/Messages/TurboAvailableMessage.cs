﻿using Newtonsoft.Json;

namespace Bot.Messages
{

  //{"msgType": "turboAvailable", "data": {
  //  "turboDurationMilliseconds": 500.0,
  //  "turboDurationTicks": 30,
  //  "turboFactor": 3.0
  //}}
  public class TurboAvailableMessage : BaseMessage<TurboAvailableMessage.TurboAvailableData>, IStateMessage
  {
    public class TurboAvailableData
    {
      [JsonProperty(PropertyName = "turboDurationMilliseconds")]
      public double TurboDurationMilliseconds { get; set; }

      [JsonProperty(PropertyName = "turboDurationTicks")]
      public int TurboDurationTicks { get; set; }

      [JsonProperty(PropertyName = "turboFactor")]
      public double TurboFactor { get; set; }
      
    }
  }
}

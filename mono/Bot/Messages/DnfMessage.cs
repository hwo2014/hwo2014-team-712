﻿using Newtonsoft.Json;

namespace Bot.Messages
{

  //{"msgType": "dnf", "data": {
  //  "car": {
  //    "name": "Rosberg",
  //    "color": "blue"
  //  },
  //  "reason": "disconnected"
  //}, "gameId": "OIUHGERJWEOI", "gameTick": 650}
  public class DnfMessage : BaseMessage<DnfMessage.DnfData>
  {
    [JsonProperty(PropertyName = "car")]
    public CarData Car { get; set; }

    [JsonProperty(PropertyName = "reason")]
    public string Reason { get; set; }

    public class DnfData
    {
      
    }
  }
}

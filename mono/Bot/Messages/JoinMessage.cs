﻿using Newtonsoft.Json;

namespace Bot.Messages
{
  
  //{"msgType": "join", "data": {
  //  "name": "Schumacher",
  //  "key": "UEWJBVNHDS"
  //}}
  public class JoinMessage : BaseMessage<JoinBotId>
  {
    public JoinMessage(JoinBotId botId) :base("join")
    {
      Data = botId;
    }
  }


  //{"msgType": "createRace", "data": {
  //  "botId": {
  //    "name": "schumacher",
  //    "key": "UEWJBVNHDS"
  //  },
  //  "trackName": "hockenheimring",
  //  "password": "schumi4ever",
  //  "carCount": 3
  //}}
  public class CreateRaceMessage : BaseMessage<RaceData>
  {
    public CreateRaceMessage(JoinBotId botId, RaceData race)
      : base("createRace")
    {
      race.BotId = botId;
      Data = race;
    }
  }


  //{"msgType": "joinRace", "data": {
  //  "botId": {
  //    "name": "keke",
  //    "key": "IVMNERKWEW"
  //  },
  //  "trackName": "hockenheimring",
  //  "password": "schumi4ever",
  //  "carCount": 3
  //}}
  public class JoinRaceMessage : BaseMessage<RaceData>
  {
    public JoinRaceMessage(JoinBotId botId, RaceData race)
      : base("joinRace")
    {
      race.BotId = botId;
      Data = race;
    }
  }



  public class RaceData
  {
    [JsonProperty(PropertyName = "botId")]
    public JoinBotId BotId { get; set; }

    [JsonProperty(PropertyName = "trackName")]
    public string TrackName { get; set; }

    [JsonProperty(PropertyName = "password")]
    public string Password { get; set; }

    [JsonProperty(PropertyName = "carCount")]
    public int CarCount { get; set; }
  }



  public class JoinBotId
  {
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    [JsonProperty(PropertyName = "key")]
    public string Key { get; set; }

  }


}

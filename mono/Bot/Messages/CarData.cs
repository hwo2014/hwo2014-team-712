﻿using Newtonsoft.Json;

namespace Bot.Messages
{
  public class CarData
  {
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    [JsonProperty(PropertyName = "color")]
    public string Color { get; set; }
  }
}

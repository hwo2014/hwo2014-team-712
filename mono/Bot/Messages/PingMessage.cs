﻿namespace Bot.Messages
{
  public class PingMessage : BaseMessage
  {
    public PingMessage() : base("ping")
    {
    }
  }

}

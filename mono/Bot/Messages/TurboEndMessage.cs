﻿namespace Bot.Messages
{
  public class TurboEndMessage : BaseMessage<CarData>, ICarStateMessage
  {
    public TurboEndMessage()
      : base("turboEnd")
    {
    }
  }
}

﻿using System;
using Newtonsoft.Json;

namespace Bot.Messages
{
  //{ "msgType":"lapFinished",
  //  "data":{
  //    "car":{
  //      "name":"Lakerfield",
  //      "color":"red"},
  //    "lapTime":{
  //      "lap":0,
  //      "ticks":589,
  //      "millis":9817},
  //    "raceTime":{
  //      "laps":1,
  //      "ticks":590,
  //      "millis":9833},
  //    "ranking":{
  //      "overall":1,
  //      "fastestLap":1}},
  //  "gameId":"d9d459f7-c737-4f91-a227-1789fdfc3882",
  //  "gameTick":590}
  public class LapFinishedMessage : BaseMessage<LapFinishedMessage.LapFinishedData>
  {
    [JsonProperty(PropertyName = "gameId")]
    public Guid GameId { get; set; }

    //[JsonProperty(PropertyName = "gameTick")]
    //public int GameTick { get; set; }

    public class LapFinishedData
    {
      [JsonProperty(PropertyName = "car")]
      public CarData Car { get; set; }

      [JsonProperty(PropertyName = "lapTime")]
      public LapFinishedLapTime LapTime { get; set; }

      // TODO
    }

    public class LapFinishedLapTime
    {
      [JsonProperty(PropertyName = "lap")]
      public int Lap { get; set; }

      [JsonProperty(PropertyName = "ticks")]
      public int Ticks { get; set; }

      [JsonProperty(PropertyName = "millis")]
      public int Millis { get; set; }

      public TimeSpan Time
      {
        get { return TimeSpan.FromMilliseconds(Millis); }
      }
    }

  }

}

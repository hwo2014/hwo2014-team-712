﻿using Newtonsoft.Json;

namespace Bot.Messages
{
  public class CarPositionsMessage : BaseMessage<CarPositionsMessage.CarPosition[]>
  {
    [JsonProperty(PropertyName = "gameId")]
    public string GameId { get; set; }

    //[JsonProperty(PropertyName = "gameTick")]
    //public int GameTick { get; set; }

    //{ "id": {
    //    "name": "Schumacher",
    //    "color": "red"
    //  },
    //  "angle": 0.0,
    //  "piecePosition": {
    //    "pieceIndex": 0,
    //    "inPieceDistance": 0.0,
    //    "lane": {
    //      "startLaneIndex": 0,
    //      "endLaneIndex": 0
    //    },
    //    "lap": 0
    //  }
    //}    
    public class CarPosition
    {
      [JsonProperty(PropertyName = "id")]
      public CarData Id { get; set; }

      [JsonProperty(PropertyName = "angle")]
      public double Angle { get; set; }

      [JsonProperty(PropertyName = "piecePosition")]
      public PiecePosition PiecePosition { get; set; }
    }

    public class PiecePosition
    {
      [JsonProperty(PropertyName = "pieceIndex")]
      public int PieceIndex { get; set; }

      [JsonProperty(PropertyName = "inPieceDistance")]
      public double InPieceDistance { get; set; }

      [JsonProperty(PropertyName = "lane")]
      public Lane Lane { get; set; }

      [JsonProperty(PropertyName = "lap")]
      public int Lap { get; set; }
    }

    public class Lane
    {
      [JsonProperty(PropertyName = "startLaneIndex")]
      public int StartLaneIndex { get; set; }

      [JsonProperty(PropertyName = "endLaneIndex")]
      public int EndLaneIndex { get; set; }
    }

  }
}

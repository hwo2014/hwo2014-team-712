﻿using System;
using Bot.Models;

namespace Bot.Display
{
  public static class DisplayTrack
  {

    public static void ToConsole(this Track track)
    {
      Console.WriteLine("Track:");

      foreach (var piece in track.Pieces)
      {
        Console.Write("{0:00} ", piece.Index);
        Console.Write("{0,8} ", piece.Way);
        Console.Write("{0:000} ", piece.AngleAbs);
        Console.Write("{0:000.00} ", piece.Length);
        Console.Write("{0:000} ", piece.Radius);

        foreach (var lane in piece.Lanes)
        {
          Console.Write("| {0:000.00} ", lane.Length);
          Console.Write("{0:000} ", lane.Radius);
        }
        
        Console.WriteLine();
      }

    }

  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Bot.Ai;

namespace Bot
{
  class Program
  {
    static void Main(string[] args)
    {
#if LEGACY_HWO_SAMPLE
      Console.WriteLine("Starting legacy bot");

      Legacy.Bot.Start(args);
#else
      Console.WriteLine("Starting reactive extensions bot");
      try
      {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        var bot1 = Engine.StartBot<BotV2Number2>(
          host, 
          port, 
          botName, 
          botKey, 
          JoinType.Default, 
          null, 
          false);

        bot1.Wait();
      }
      catch (Exception exception)
      {
        Console.WriteLine(@"Exception: {0}", exception.Message);
        Console.WriteLine(@"Stacktrace:");
        Console.WriteLine(exception.StackTrace);
      }

#endif
      Console.WriteLine("Bye");
    }
  }
}

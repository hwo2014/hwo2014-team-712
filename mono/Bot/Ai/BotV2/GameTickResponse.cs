﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot.Ai.BotV2
{
  public class GameTickResponse
  {
    public int GameTick { get; set; }
    public List<Option> Options { get; private set; }
    public Messages.CarPositionsMessage CarPositionsMessage { get; set; }
    public Messages.BaseMessage SendMessage { get; set; }
    public PositionAnalyzerCarTick CarInfo { get; set; }

    public GameTickResponse()
    {
      Options = new List<Option>();

    }


    public class Option
    {
      public GameTickResponseMessage Message { get; set; }
      public GameTickResponsePriority Priority { get; set; }
      public double Throttle { get; set; }
      public SwitchLane SwitchLane { get; set; }
      public string TurboMessage { get; set; }

      public Option()
      {
        Priority = GameTickResponsePriority.IfNothingElse;

      }

    }
  }
}

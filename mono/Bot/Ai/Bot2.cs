﻿using System;
using System.Reactive.Linq;
using Bot.Connection;
using Bot.Messages;

namespace Bot.Ai
{
  public class Bot2 : BotBase
  {

    private PositionAnalyzer _analyzer;
    public PositionAnalyzer Analyzer
    {
      get
      {
        if (_analyzer == null)
          _analyzer = new PositionAnalyzer(Race, BotColor);
        return _analyzer;
      }
    }

    public Bot2(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {
    }

    protected override void Setup(IObservable<BaseMessage> messages)
    {
      messages.OfType<GameStartMessage>();

      var gameEndSubscription = messages.OfType<GameEndMessage>().Subscribe(HandleGameEnd);
      _disposables.Add(gameEndSubscription);

      var carPositionsSubscription = messages.OfType<CarPositionsMessage>().Subscribe(HandleCarPositions);
      _disposables.Add(carPositionsSubscription);
    }

    private void HandleGameEnd(GameEndMessage message)
    {
      Analyzer.Reset();
    }

    private void HandleCarPositions(CarPositionsMessage message)
    {
      var throttle = 1d;

      Analyzer.Update(message);

      var carAnalyzer = Analyzer[BotColor];
      var currentSpeed = carAnalyzer.LastTick.Speed;

      var relativeTrackReader = carAnalyzer.GetTrackReader();

      var currentPiece = relativeTrackReader[0];
      var nextPiece = relativeTrackReader[1];

      // voor bocht
      if (currentPiece.Way == Way.Straight && nextPiece.Way != Way.Straight)
      {
        var distanceToCorner = currentPiece[carAnalyzer.LastTick.LaneStart, carAnalyzer.LastTick.LaneEnd].Length -
                               carAnalyzer.LastTick.InPieceDistance;

        var targetSpeed = 3d;//carAnalyzer.PieceInfo[nextPiece.Index].LastExitSpeed;
        var acc = 0.162d;

        var breakDistance = (currentSpeed * currentSpeed- targetSpeed * targetSpeed) / 2 * acc;

        Console.WriteLine(string.Format(@"{0:000.000} {1:000.000} {2:000.000} {3:000.000} ", breakDistance, distanceToCorner, currentSpeed, targetSpeed));

        if (distanceToCorner < breakDistance)
          if (currentSpeed >= targetSpeed)
            throttle = 0;
      }
      // in bocht
      else if (currentPiece.Way != Way.Straight)
      {
        if (Math.Abs(carAnalyzer.LastTick.Angle) > 15)
          throttle = 0;
      }

      carAnalyzer.PieceInfo[currentPiece.Index].LastExitSpeed = currentSpeed;
      carAnalyzer.LastTick.UpdateSendThrottle(throttle);

      SendMessage(new ThrottleMessage()
      {
        Data = throttle,
        GameTick = message.GameTick
      });
    }



  }
}

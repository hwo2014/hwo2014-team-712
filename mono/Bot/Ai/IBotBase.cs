using System;
using System.Threading.Tasks;
using Bot.Models;

namespace Bot.Ai
{
  public interface IBotBase : IDisposable
  {
    Race Race { get; set; }
    string BotName { get; set; }
    string BotColor { get; set; }
    event BotBase.ObservableCreatedHandler ObservableCreated;
    Task Play();
  }

  public interface IBotAnalyzerBase : IBotBase
  {
    PositionAnalyzer Analyzer { get; }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Bot.Ai.BotV2;
using Bot.Connection;
using Bot.Display;
using Bot.Messages;
using Bot.Models;
using Bot.Parser;
using Newtonsoft.Json;

namespace Bot.Ai
{
  public abstract class BotV2Base : IBotAnalyzerBase
  {
    private readonly CompositeDisposable _compositeDisposable;

    private readonly IConnection _connection;
    private readonly BaseMessage _joinMessage;

    public bool DisplayTrackInfo { get; set; }

    public Race Race { get; set; }
    public string BotName { get; set; }
    public string BotColor { get; set; }
    public PositionAnalyzer Analyzer { get; set; }
    public SwitchLane SwitchLaneCommandSendToServer { get; private set; }



    public event BotBase.ObservableCreatedHandler ObservableCreated;

    protected BotV2Base(IConnection connection, BaseMessage joinMessage)
    {
      _compositeDisposable = new CompositeDisposable();
      _connection = connection;
      _joinMessage = joinMessage;
    }



    public Task Play()
    {
      var taskCompletionSource = new TaskCompletionSource<bool>();
      var messages =
        GetObservableMessages()
          .Finally(() => taskCompletionSource.SetResult(true))
          .Publish()
          .RefCount();

      TrackDisposable(messages.OfType<GameInitMessage>().Take(1).Subscribe(HandleGameInit));
      TrackDisposable(messages.OfType<CarPositionsMessage>().Subscribe(HandleCarPositions));
      TrackDisposable(messages.OfType<GameStartMessage>().Subscribe(HandleGameStart));
      TrackDisposable(messages.OfType<YourCarMessage>().Take(1).Subscribe(HandleYourCar));
      TrackDisposable(messages.OfType<CrashMessage>().Take(1).Subscribe(HandleCrash));
      TrackDisposable(messages.OfType<SpawnMessage>().Take(1).Subscribe(HandleSpawn));
      TrackDisposable(messages.OfType<TurboAvailableMessage>().Take(1).Subscribe(HandleTurboAvailable));
      TrackDisposable(messages.OfType<TurboStartMessage>().Take(1).Subscribe(HandleTurboStart));
      TrackDisposable(messages.OfType<TurboEndMessage>().Take(1).Subscribe(HandleTurboEnd));
      TrackDisposable(messages.OfType<LapFinishedMessage>().Subscribe(HandleLapFinished));
      TrackDisposable(messages.OfType<FinishMessage>().Subscribe(HandleFinish));
      TrackDisposable(messages.OfType<GameEndMessage>().Subscribe(HandleGameEnd));
      TrackDisposable(messages.OfType<TournamentEndMessage>().Subscribe(HandleTournamentEnd));

      Setup(messages);

      var gameTickRespones = messages
        .Where(IsGameTickResponseNeeded)
        .Select(CalculateGameTickResponse)
        .Publish()
        .RefCount();

      // Hook for WPF
      if (ObservableCreated != null)
        ObservableCreated(this, messages, gameTickRespones);

      TrackDisposable(gameTickRespones.Subscribe(SendMessage));

      SendMessage(_joinMessage);

      return taskCompletionSource.Task;
    }

    protected virtual void Setup(IObservable<BaseMessage> messages)
    {
      
    }


    private void HandleYourCar(YourCarMessage message)
    {
      if (message.Data == null)
      {
        // Invalid protocol...
        BotName = "Unknown";
        BotColor = "transparent";
      }
      else
      {
        BotName = message.Data.Name;
        BotColor = message.Data.Color;
      }
    }

    protected virtual void HandleGameInit(GameInitMessage message)
    {
      var parser = new GameInitParser(message);
      var race = parser.Parse();
      if (Race == null || Race.QuickRace || Race.Track.Id != message.Data.Race.Track.Id)
      {
        Race = race;

        new ShortestLaneSearcher().UpdateRace(Race);

        Analyzer = new PositionAnalyzer(Race, BotColor);
      }
      else
      {
        Race.Laps = race.Laps;
        Race.MaxLapTimeMs = race.MaxLapTimeMs;
        Race.QuickRace = race.QuickRace;

        Analyzer.Reset();
      }

      SwitchLaneCommandSendToServer = SwitchLane.None;

      if (DisplayTrackInfo)
        Race.Track.ToConsole();
    }

    private void HandleCarPositions(CarPositionsMessage message)
    {
      Analyzer.Update(message);
    }

    public void HandleGameStart(GameStartMessage message)
    {
      // todo: gametick SendMessage(new PingMessage());
    }



    protected virtual bool IsGameTickResponseNeeded(BaseMessage message)
    {
      var carPositionsMessage = message as CarPositionsMessage;
      if (carPositionsMessage != null)
      {
        return carPositionsMessage.GameTick > 0;
      }

      var gameStartMessage = message as GameStartMessage;
      if (gameStartMessage != null)
        return true;

      return false;
    }

    protected virtual GameTickResponse CalculateGameTickResponse(BaseMessage message)
    {
      var result = new GameTickResponse();
      result.GameTick = message.GameTick;

      result.CarInfo = Analyzer.Self.LastTick;

      if (result.GameTick <= 3)
      {
        result.Options.Add(new GameTickResponse.Option()
        {
          Priority = GameTickResponsePriority.ForceForMath,
          Message = GameTickResponseMessage.Throttle,
          Throttle = 1.0d
        });
      }
      else if (!Analyzer.Self.FCentrifugalSlipCalculated)
      {
        var trackReader = Analyzer.Self.GetTrackReader();
        var tickInfo = Analyzer.Self.LastTick;

        if (trackReader[0].Way == Way.Straight)
        {
          if (trackReader[1].Way != Way.Straight)
          {
            if ((tickInfo.InPieceDistance + 3*tickInfo.Speed) >= trackReader[1].Length)
            {
              result.Options.Add(new GameTickResponse.Option()
              {
                Priority = GameTickResponsePriority.ForceForMath,
                Message = GameTickResponseMessage.Throttle,
                Throttle = Analyzer.Self.CalculateThrottleForSpeed(tickInfo.Speed)
              });
              //Console.WriteLine("S: {0:000.000000}", tickInfo.Speed);
            }
          }
        }
        else if (trackReader[0].Way != Way.Straight)
        {
          result.Options.Add(new GameTickResponse.Option()
          {
            Priority = GameTickResponsePriority.ForceForMath,
            Message = GameTickResponseMessage.Throttle,
            Throttle = Analyzer.Self.CalculateThrottleForSpeed(tickInfo.Speed)
          });
          //Console.WriteLine("S: {0:000.000000}", tickInfo.Speed);
        }
      }

      if (result.Options.Count == 0)
      {
        result.Options.Add(new GameTickResponse.Option()
        {
          Priority = GameTickResponsePriority.IfNothingElse,
          Message = GameTickResponseMessage.Throttle,
          Throttle = 0.5d
        });
      }

      // Reset switchlane state
      var lastTick = Analyzer.Self.LastTick;
      if (lastTick.LaneStart != lastTick.LaneEnd)
        SwitchLaneCommandSendToServer = SwitchLane.None;

      return result;
    }



    private void HandleCrash(CrashMessage message)
    {
      LogLine(@"{0} crashed in {1} car", message.Data.Name, message.Data.Color);
      Analyzer.QueueStateMessage(message);
    }

    private void HandleSpawn(SpawnMessage message)
    {
      LogLine(@"{0} spawned in {1} car", message.Data.Name, message.Data.Color);
      Analyzer.QueueStateMessage(message);
    }

    private void HandleTurboAvailable(TurboAvailableMessage message)
    {
      LogLine(@"Turbo available for all cars");
      Analyzer.QueueStateMessage(message);
    }

    private void HandleTurboStart(TurboStartMessage message)
    {
      LogLine(@"Turbo active for {0} car", message.Data.Color);
      Analyzer.QueueStateMessage(message);
    }

    private void HandleTurboEnd(TurboEndMessage message)
    {
      LogLine(@"Turbo ended for {0} car", message.Data.Color);
      Analyzer.QueueStateMessage(message);
    }




    private void HandleLapFinished(LapFinishedMessage message)
    {
      //if (message.Data.Car.Color != BotColor)
      //  return;

      //Console.WriteLine(@"Lap completed by {0} in {1:mm\:ss\.fff}.", message.Data.Car.Color, message.Data.LapTime.Time);
    }

    private void HandleFinish(FinishMessage message)
    {
      //SendMessage(new PingMessage());
    }

    private void HandleGameEnd(GameEndMessage message)
    {
      //SendMessage(new PingMessage());
    }

    private void HandleTournamentEnd(TournamentEndMessage message)
    {
      //SendMessage(new PingMessage());
    }

    private IObservable<BaseMessage> GetObservableMessages()
    {
      var messageReader = new MessageReader(_connection);

      //The enumerator will be scheduled on separate thread
      var observable = messageReader
                        .ObservableMessages()
                        .ToObservable(System.Reactive.Concurrency.ThreadPoolScheduler.Instance)
                        .Do(LogNext, LogException, LogFinally)
                        ;

      return observable;
    }


    #region Observable Do and Logging

    private void LogNext(BaseMessage message)
    {
      //Log(".");
    }

    private void LogException(Exception exception)
    {
      LogLine("EXCEPTION [{0}]", exception.Message);
    }

    private void LogFinally()
    {
      LogLine("FINALLY");
    }

    private bool _logLineIsEmpty = true;
    public void Log(string message, params object[] args)
    {
      _logLineIsEmpty = false;
      Console.Write(message, args);
    }

    public void LogLine(string message, params object[] args)
    {
      if (!_logLineIsEmpty)
        Console.WriteLine();
      _logLineIsEmpty = true;
      Console.WriteLine(message, args);
    }

    #endregion

    public double GetMaxThrottle(GameTickResponse gameTickResponse)
    {
      //return gameTickResponse.CarInfo.TurboActive ? gameTickResponse.CarInfo.TurboActiveData.TurboFactor : 1d;
      return gameTickResponse.CarInfo.TurboActiveFactor;
    }

    public double GetClippedThrottle(GameTickResponse gameTickResponse, double throttle)
    {
      var maxThrottle = GetMaxThrottle(gameTickResponse);
      if (throttle > maxThrottle)
        throttle = maxThrottle;
      if (throttle < 0)
        throttle = 0;
      return throttle;
    }

    private GameTickResponse _lastGameTickResponse;
    public void SendMessage(GameTickResponse gameTickResponse)
    {
      var option = gameTickResponse.Options.OrderBy(o => o.Priority).FirstOrDefault();
      if (option == null)
        return;

      createMessage:

      BaseMessage message;
      switch (option.Message)
      {
        case GameTickResponseMessage.Throttle:
          double unCorrectedThrottle;
          var sendThrottle = CheckAndOrCorrectThrottle(gameTickResponse, option.Throttle, out unCorrectedThrottle);
          message = new ThrottleMessage()
          {
            GameTick = gameTickResponse.GameTick,
            Data = sendThrottle
          };

          // Option for sending other message instead of throttle if throttle keeps the same
          if (_lastGameTickResponse != null)
          {
            var lastThrottleMessage = _lastGameTickResponse.SendMessage as ThrottleMessage;
            if (lastThrottleMessage != null && option.Throttle == lastThrottleMessage.Data)
            {
              var otherOption = gameTickResponse.Options.Where(o => o.Message != GameTickResponseMessage.Throttle).OrderBy(o => o.Priority).FirstOrDefault();
              if (otherOption != null)
              {
                option = otherOption;
                goto createMessage;
              }
            }
          }

          gameTickResponse.CarInfo.UpdateSendThrottle(unCorrectedThrottle);
          break;

        case GameTickResponseMessage.Switch:
          if (option.SwitchLane == SwitchLane.None)
          {
            message = new PingMessage()
            {
              GameTick = gameTickResponse.GameTick
            };
            Log("Invalid switch lane command: {0}", option.SwitchLane);
          }
          else
          {
            message = new SwitchLaneMessage()
            {
              GameTick = gameTickResponse.GameTick,
              Data = option.SwitchLane
            };
            Log("Send switch lane to: {0}", option.SwitchLane);
          }
          SwitchLaneCommandSendToServer = option.SwitchLane;
          gameTickResponse.CarInfo.UpdateSendThrottle();
          break;

        case GameTickResponseMessage.Turbo:
          message = new TurboMessage()
          {
            GameTick = gameTickResponse.GameTick,
            Data = string.IsNullOrEmpty(option.TurboMessage) ? "Pow pow pow pow pow" : option.TurboMessage
          };
          gameTickResponse.CarInfo.UpdateSendThrottle();
          Log("Activate turbo: {0}", ((TurboMessage)message).Data);
          break;

        case GameTickResponseMessage.Stop:
          message = new ThrottleMessage()
          {
            GameTick = gameTickResponse.GameTick,
            Data = 2
          };
          Log("Send invalid throttle => stop bot");
          break;

        default:
          throw new ArgumentOutOfRangeException();
      }

      gameTickResponse.SendMessage = message;
      _lastGameTickResponse = gameTickResponse;

      var json = JsonConvert.SerializeObject(message);
      _connection.Write(json);
    }

    private double CheckAndOrCorrectThrottle(GameTickResponse gameTickResponse, double optionThrottle, out double unCorrectedThrottle)
    {
      unCorrectedThrottle = GetClippedThrottle(gameTickResponse, optionThrottle);
      var sendThrottle = unCorrectedThrottle / GetMaxThrottle(gameTickResponse);
      if (sendThrottle > 1d)
        sendThrottle = 1d;
      if (sendThrottle < 0d)
        sendThrottle = 0d;
      return sendThrottle;
    }

    public void SendMessage(BaseMessage message)
    {
      var json = JsonConvert.SerializeObject(message);
      _connection.Write(json);
    }


    protected void TrackDisposable(IDisposable disposable)
    {
      _compositeDisposable.Add(disposable);
    }

    public void Dispose()
    {
      _compositeDisposable.Dispose();
    }

  }
}

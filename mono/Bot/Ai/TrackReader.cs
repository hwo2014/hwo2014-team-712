﻿using Bot.Models;

namespace Bot.Ai
{
  public class TrackReader
  {
    private readonly Track _track;

    public TrackReader(Track track)
    {
      _track = track;
    }

    public int this[int lap, int pieceIndex]
    {
      get { return lap*_track.Pieces.Length + pieceIndex; }
    }

    public TrackPiece this[int pieceIndex]
    {
      get
      {
        var index = pieceIndex % _track.Pieces.Length;
        if (index < 0)
          index += _track.Pieces.Length;
        return _track.Pieces[index];
      }
    }



    public class RelativeTrackReader
    {
      private readonly TrackReader _trackReader;
      private readonly int _position;

      public int CurrentPiece { get { return _position; } }

      public RelativeTrackReader(TrackReader trackReader, int racePieceIndex)
      {
        _trackReader = trackReader;
        _position = racePieceIndex;
      }

      public RelativeTrackReader(TrackReader trackReader, int lap, int pieceIndex)
        : this(trackReader, trackReader[lap, pieceIndex])
      {
      }

      public TrackPiece this[int relativeIndex]
      {
        get { return _trackReader[_position + relativeIndex]; }
      }

    }
    
  }



}

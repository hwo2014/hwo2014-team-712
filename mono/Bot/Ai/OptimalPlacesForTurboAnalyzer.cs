﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bot.Models;

namespace Bot.Ai
{
  public class OptimalPlacesForTurboAnalyzer
  {
    private StraightLength[] _straightStartPieceIndexes = new StraightLength[0];
    private StraightLength[] _bestStraightStartPieceIndexes = new StraightLength[0];

    public Race Race { get; private set; }

    public OptimalPlacesForTurboAnalyzer(Models.Race race)
    {
      Race = race;


      if (Race.Track.Pieces.Any(p => p.Way == Way.Straight))
      {
        _straightStartPieceIndexes = SearchStraightPieces().ToArray();

        var x = _straightStartPieceIndexes.GroupBy(p => p.Length).OrderByDescending(p => p.Key).ToArray();

        _bestStraightStartPieceIndexes = x.First().ToArray();
      }
    }



    public bool IsOptimal(PositionAnalyzerCarTick tickInfo)
    {
      var currentPieceIndex = tickInfo.PieceIndex;

      if (_bestStraightStartPieceIndexes.Length > 0)
        return _bestStraightStartPieceIndexes.Any(p => p.PieceIndex == currentPieceIndex);

      // TODO: if no straights, find corner with biggest radius...

      return false;
    }


    private IEnumerable<StraightLength> SearchStraightPieces()
    {
      var track = Race.Track;

      StraightLength straightLength = null;
      for (int i = 0; i < track.Pieces.Length; i++)
      {
        var piece = track.Pieces[i];
        if (piece.Way == Way.Straight)
        {
          if (straightLength == null)
            straightLength = new StraightLength() { PieceIndex = i };

          straightLength.Pieces += 1;
          straightLength.Length += piece.Length;
          continue;
        }

        if (straightLength == null)
          continue;

        // Skip first straight if last piece is straight
        if (straightLength.PieceIndex != 0 || track.Pieces.Last().Way != Way.Straight)
          yield return straightLength;

        straightLength = null;
      }

      if (straightLength == null)
        yield break;

      foreach (var piece in track.Pieces)
      {
        if (piece.Way != Way.Straight)
          break;

        straightLength.Pieces += 1;
        straightLength.Length += piece.Length;
      }

      yield return straightLength;
    }


    public class StraightLength
    {
      public int PieceIndex { get; set; }
      public int Pieces { get; set; }
      public double Length { get; set; }
    }

  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bot.Models;

namespace Bot.Ai
{
  public class MaxSpeedForCornerAnalyzer
  {
    private Dictionary<double, CornerPrediction> _cornerPredictions = new Dictionary<double, CornerPrediction>();
    public Race Race { get; set; }
    public PositionAnalyzerCar Car { get; set; }


    
    public MaxSpeedForCornerAnalyzer(Race race, PositionAnalyzerCar car)
    {
      Race = race;
      Car = car;
    }


    public double this[TrackPiece piece, TrackPieceLane lane]
    {
      get
      {
        var radius = lane.Radius;
        var maxCentriForce = 0.32 + 0.1;

        switch (Race.Track.Id)
        {
          case "elaeintarha":

            //if (radius == 30d)
            maxCentriForce += 0.1;

            if (Car.LastTick.PieceIndex < 18)
              maxCentriForce += 0.08;

            if (Car.LastTick.PieceIndex >= 18 && Car.LastTick.PieceIndex < 21)
              maxCentriForce += 0.02;

            if (Car.LastTick.PieceIndex >= 21 && Car.LastTick.PieceIndex < 27)
              maxCentriForce += 0.08;


            break;
        }


        return Math.Sqrt(maxCentriForce * radius);
      }
    }



    public void Update()
    {
      var lastTick = Car.LastTick;


      if (lastTick.MaybeBumpedOrSwitch)
        return;

    }


    private CornerPrediction GetCornerPrediction(double radius)
    {
      var roundedRadius = Math.Round(radius, 5);

      //_cornerPredictions.Add();
      throw new NotImplementedException();
    }


    public class CornerPrediction
    {
      public double RadiusRounded { get; set; }

      public double EstimatedSpeed { get; set; }

    }

  }
}

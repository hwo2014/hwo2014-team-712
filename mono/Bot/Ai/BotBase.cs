﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Bot.Ai.BotV2;
using Bot.Connection;
using Bot.Display;
using Bot.Messages;
using Bot.Models;
using Bot.Parser;
using Newtonsoft.Json;

namespace Bot.Ai
{
  public abstract class BotBase : IBotBase
  {
    protected readonly CompositeDisposable _disposables;
    private readonly IConnection _connection;
    private readonly BaseMessage _joinMessage;

    public Race Race { get; set; }
    public string BotName { get; set; }
    public string BotColor { get; set; }

    protected BotBase(IConnection connection, BaseMessage joinMessage)
    {
      _disposables = new CompositeDisposable();
      _connection = connection;
      _joinMessage = joinMessage;
    }
    
    // Wpf
    public delegate void ObservableCreatedHandler(IBotBase sender, IObservable<BaseMessage> observable, IObservable<GameTickResponse> gameTickResponses);
    public event ObservableCreatedHandler ObservableCreated;

    public Task Play()
    {
      var taskCompletionSource = new TaskCompletionSource<bool>();
      var messages = 
        GetObservableMessages()
          .Finally(() => taskCompletionSource.SetResult(true))
          .Publish()
          .RefCount();

      var gameInitSubscription = messages.OfType<GameInitMessage>().Take(1).Subscribe(HandleGameInit);
      _disposables.Add(gameInitSubscription);

      var gameStartSubscription = messages.OfType<GameStartMessage>().Subscribe(HandleGameStart);
      _disposables.Add(gameStartSubscription);

      var yourCarSubscription = messages.OfType<YourCarMessage>().Take(1).Subscribe(HandleYourCar);
      _disposables.Add(yourCarSubscription);

      var lapFinishedSubscription = messages.OfType<LapFinishedMessage>().Subscribe(HandleLapFinished);
      _disposables.Add(lapFinishedSubscription);

      var finishSubscription = messages.OfType<FinishMessage>().Subscribe(HandleFinish);
      _disposables.Add(finishSubscription);

      var gameEndSubscription = messages.OfType<GameEndMessage>().Subscribe(HandleGameEnd);
      _disposables.Add(gameEndSubscription);

      var tournamentEndSubscription = messages.OfType<TournamentEndMessage>().Subscribe(HandleTournamentEnd);
      _disposables.Add(tournamentEndSubscription);

      Setup(messages);

      if (ObservableCreated != null)
        ObservableCreated(this, messages, null);

      SendMessage(_joinMessage);

      return taskCompletionSource.Task;
    }

    protected abstract void Setup(IObservable<BaseMessage> messages);

    protected virtual void HandleGameInit(GameInitMessage message)
    {
      var parser = new GameInitParser(message);
      Race = parser.Parse();

      //Race.Track.ToConsole();
    }

    public void HandleGameStart(GameStartMessage message)
    {
      // todo: gametick SendMessage(new PingMessage());
    }

    private void HandleYourCar(YourCarMessage message)
    {
      if (message == null || message.Data == null)
      {
        BotName = "Unknown";
        BotColor = "transparent";
      }
      else
      {
        BotName = message.Data.Name;
        BotColor = message.Data.Color;
      }
    }

    private void HandleLapFinished(LapFinishedMessage message)
    {
      //if (message.Data.Car.Color != BotColor)
      //  return;

      //Console.WriteLine(@"Lap completed by {0} in {1:mm\:ss\.fff}.", message.Data.Car.Color, message.Data.LapTime.Time);
    }

    private void HandleFinish(FinishMessage message)
    {
      SendMessage(new PingMessage());
    }

    private void HandleGameEnd(GameEndMessage message)
    {
      SendMessage(new PingMessage());
    }

    private void HandleTournamentEnd(TournamentEndMessage message)
    {
      SendMessage(new PingMessage());
    }

    private IObservable<BaseMessage> GetObservableMessages()
    {
      var messageReader = new MessageReader(_connection);

      //The enumerator will be scheduled on separate thread
      var observable = messageReader
                        .ObservableMessages()
                        .ToObservable(System.Reactive.Concurrency.ThreadPoolScheduler.Instance)
                        .Do(LogNext, LogException, LogFinally);
      
      return observable;
    }

    private void LogNext(BaseMessage message)
    {
      //Console.Write(".");
    }

    private void LogException(Exception exception)
    {
      Console.Write("EXCEPTION");
    }

    private void LogFinally()
    {
      Console.Write("FINALLY");
    }

    public void SendMessage(BaseMessage message)
    {
      var json = JsonConvert.SerializeObject(message);
      _connection.Write(json);
    }

    public void Dispose()
    {
      _disposables.Dispose();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bot.Ai.BotV2;
using Bot.Connection;
using Bot.Messages;

namespace Bot.Ai
{
  public class BotV2Number1 : BotV2Base
  {


    public BotV2Number1(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {

    }


    protected override GameTickResponse CalculateGameTickResponse(BaseMessage message)
    {
      var result = base.CalculateGameTickResponse(message);
      var tickInfo = Analyzer.Self.LastTick;

      //tickInfo.Speed;
      //tickInfo.Acceleration;
      //tickInfo.Angle;

      //var throttle = 0.5d;
      //if (tickInfo.GameTick >= 5 && tickInfo.GameTick < 10)
      //{
      //  result.Options.Add(new GameTickResponse.Option()
      //  {
      //    Priority = GameTickResponsePriority.Optimal,
      //    Message = GameTickResponseMessage.Throttle,
      //    Throttle = 0.1d
      //  });
      //  throttle = 0.1d;
      //}



      //// Vnext = V0 + (F*Throttle - V0)/(5*F)
      //var factor = 10;
      //tickInfo.SpeedNextExpected = tickInfo.Speed + (factor * throttle - tickInfo.Speed)/(factor*5);


      // Vnext = V0 + Throttle/Power - V0*Drag

      // Power = Throttle/Vnext (by Tick0 stel Drag == 0)
      // Drag = (V0 + Throttle/Power - Vnext)/V0
      // Drag = 1 + (Throttle/Power - Vnext)/V0


      var throttle = Analyzer.Self.CalculateThrottleForSpeed(9.8d);


      result.Options.Add(new GameTickResponse.Option()
      {
        Priority = GameTickResponsePriority.Optimal,
        Message = GameTickResponseMessage.Throttle,
        Throttle = throttle
      });


      tickInfo.SpeedNextExpected = tickInfo.Speed + throttle / Analyzer.Self.Power - tickInfo.Speed * Analyzer.Self.Drag;




      var expectedSpeed = 0d;
      if (tickInfo.PreviousTick != null)
        expectedSpeed = tickInfo.PreviousTick.SpeedNextExpected;
      //LogLine(@"Tick: {0}, Speed: {1}, Exp:{2}, Acc: {3}", tickInfo.GameTick, tickInfo.Speed, expectedSpeed, tickInfo.Acceleration);

      if (tickInfo.GameTick > 25)
      {
        //System.Threading.Thread.Sleep(5000);
      }
      if (tickInfo.TurboAvailable)
      {
        result.Options.Add(new GameTickResponse.Option()
        {
          Message = GameTickResponseMessage.Turbo
        });
      }

      return result;
    }
  }
}

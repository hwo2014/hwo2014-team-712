﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using Bot.Models;

namespace Bot.Ai
{
  public class ShortestLaneSearcher
  {
    public void UpdateRace(Models.Race race)
    {
      UpdateTrackLaneLength(race);
      UpdateTrackSwitchPieces(race);
    }

    private void UpdateTrackLaneLength(Race race)
    {
      foreach (var lane in race.Lanes)
        lane.TotalLength = race.Track.Pieces.Sum(p => p.Lanes[lane.Index].Length);
    }

    private void UpdateTrackSwitchPieces(Race race)
    {
      var track = race.Track;
      var pieces = track.Pieces.Length;
      var trackReader = new TrackReader(track);

      var firstSwitchInTrackIndex = -1;
      for (int i = 0; i < pieces; i++)
      {
        if (!trackReader[i].Switch)
          continue;
        firstSwitchInTrackIndex = i;
        break;
      }

      if (firstSwitchInTrackIndex < 0)
        return;

      var currentLanes = race.Lanes.Select(lane => new LaneInfo(lane)).ToArray();

      TrackPiece currentSwitchPiece = null;
      for (int i = firstSwitchInTrackIndex; i <= (pieces + firstSwitchInTrackIndex); i++)
      {
        var piece = trackReader[i];

        if (piece.Switch)
        {
          if (currentSwitchPiece != null)
          {
            foreach (var currentLane in currentLanes)
            {
              var lane = currentSwitchPiece.Lanes.First(l => l.Index == currentLane.Lane.Index);
              lane.DistanceToNextSwitch = currentLane.Length;
              
              // Reset for next part
              currentLane.Length = 0;
            }
          }
          currentSwitchPiece = piece;
        }
        else
        {
          foreach (var currentLane in currentLanes)
          {
            var lane = piece.Lanes.First(l => l.Index == currentLane.Lane.Index);
            currentLane.Length += lane.Length;
          }
        }
      }
    }



    public class LaneInfo
    {
      public Lane Lane { get; private set; }
      
      public double Length { get; set; }

      public LaneInfo(Lane lane)
      {
        Lane = lane;
      }
    }

  }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Bot.Messages;
using Bot.Models;

namespace Bot.Ai
{
  public class PositionAnalyzer
  {
    private readonly string _selfColor;
    private readonly Dictionary<string, PositionAnalyzerCar> _cars = new Dictionary<string, PositionAnalyzerCar>();
    private readonly TrackReader _trackReader;
    private readonly List<IStateMessage> _stateMessageQueue = new List<IStateMessage>(); 

    public Race Race { get; set; }

    public PositionAnalyzerCar this[string color]
    {
      get
      {
        PositionAnalyzerCar result;
        if (_cars.TryGetValue(color, out result))
          return result;
        return null;
      }
    }

    public PositionAnalyzerCar Self
    {
      get { return this[_selfColor]; }
    }


    public PositionAnalyzer(Race race, string selfColor)
    {
      _selfColor = selfColor;
      Race = race;
      _trackReader = new TrackReader(Race.Track);

      Init();
    }

    private void Init()
    {
      foreach (var car in Race.Cars)
        _cars.Add(car.Color, new PositionAnalyzerCar(car, _trackReader, Race.Track.Pieces.Length));

      Self.IsSelf = true;
    }

    public void Reset()
    {
      _stateMessageQueue.Clear();
      foreach (var car in _cars)
      {
        car.Value.Ticks.Clear();
      }
    }

    public void QueueStateMessage(IStateMessage message)
    {
      _stateMessageQueue.Add(message);
    }

    public void Update(CarPositionsMessage message)
    {
      foreach (var carPosition in message.Data)
      {
        var car = this[carPosition.Id.Color];
        if (car == null)
          continue;

        var tick = new PositionAnalyzerCarTick()
        {
          PreviousTick = car.LastTick,
          Lap = carPosition.PiecePosition.Lap,
          PieceIndex = carPosition.PiecePosition.PieceIndex,
          LaneStart = carPosition.PiecePosition.Lane.StartLaneIndex,
          LaneEnd = carPosition.PiecePosition.Lane.EndLaneIndex,
          InPieceDistance = carPosition.PiecePosition.InPieceDistance,
          Angle = carPosition.Angle,
          GameTick = message.GameTick
        };

        tick.RacePieceIndex = _trackReader[tick.Lap, tick.PieceIndex];

        #region Handle Turbo/Crash/Spawn state messages

        var stateMessagesForCar = _stateMessageQueue
          .OfType<ICarStateMessage>()
          .Where(m => m.Data.Color == car.Car.Color)
          .ToArray();

        // TurboAvailable
        var turboAvailableMessage = _stateMessageQueue.OfType<TurboAvailableMessage>().FirstOrDefault();
        if (turboAvailableMessage != null)
        {
          tick.TurboAvailable = true;
          tick.TurboAvailableData = turboAvailableMessage.Data;
        }
        else if (tick.PreviousTick != null && tick.PreviousTick.TurboAvailable)
        {
          tick.TurboAvailable = true;
          tick.TurboAvailableData = tick.PreviousTick.TurboAvailableData;
        }
        

        // TurboStart
        var turboStartMessage = stateMessagesForCar.OfType<TurboStartMessage>().FirstOrDefault();
        if (turboStartMessage != null)
        {
          tick.TurboActive = true;
          tick.TurboActiveData = tick.TurboAvailableData;
          tick.TurboActiveTickRemaining = tick.TurboActiveData.TurboDurationTicks;
          tick.TurboActiveFactor = tick.TurboActiveData.TurboFactor;

          tick.TurboAvailable = false;
          tick.TurboAvailableData = null;
        }
        else if (tick.PreviousTick != null && tick.PreviousTick.TurboActive)
        {
          tick.TurboActive = true;
          tick.TurboActiveData = tick.PreviousTick.TurboActiveData;
          tick.TurboActiveTickRemaining = tick.PreviousTick.TurboActiveTickRemaining - 1;
          tick.TurboActiveFactor = tick.PreviousTick.TurboActiveFactor;
        }

        // TurboEnd
        var turboEndMessage = stateMessagesForCar.OfType<TurboEndMessage>().FirstOrDefault();
        if (turboEndMessage != null)
        {
          tick.TurboActive = false;
          tick.TurboActiveData = null;
          tick.TurboActiveTickRemaining = 0;
          tick.TurboActiveFactor = 1d;
        }

        // Crash
        var crashMessage = stateMessagesForCar.OfType<CrashMessage>().FirstOrDefault();
        if (crashMessage != null)
        {
          tick.Crashed = true;
          tick.TurboActiveData = tick.TurboAvailableData;

          if (tick.TurboActive)
          {
            tick.TurboActive = false;
            tick.TurboActiveData = null;
            tick.TurboActiveTickRemaining = 0;
            tick.TurboActiveFactor = 1d;
          }
        }
        
        // Spawn
        var spawnMessage = stateMessagesForCar.OfType<SpawnMessage>().FirstOrDefault();
        if (spawnMessage != null)
          tick.Crashed = false;

        #endregion

        car.Add(tick);
      }

      _stateMessageQueue.Clear();
    }

  }

  public class PositionAnalyzerCar
  {
    private readonly TrackReader _trackReader;
    private List<PositionAnalyzerCarTick> _ticks = new List<PositionAnalyzerCarTick>();
    public List<PositionAnalyzerCarTick> Ticks { get { return _ticks; } }

    public Car Car { get; set; }
    public CarState State { get; set; }
    public double Power { get; set; }
    public double Drag { get; set; }
    /// <summary>
    /// Power * Drag
    /// </summary>
    public double Drag2 { get; set; }
    public double Mass { get; set; }
    public double FCentrifugalSlip { get; set; }

    //public int RacePieceIndex { get; set; }
    //public double Angle { get; set; }
    //public double Speed { get; set; }
    //public double Acceleration { get; set; }

    public PositionAnalyzerCarTick LastTick { get; set; }

    public PositionAnalyzerCarPieceInfo[] PieceInfo { get; set; }
    public bool IsSelf { get; set; }

    public bool FCentrifugalSlipCalculated { get { return _fCentrifugalSlipCalculated; } }

    public PositionAnalyzerCar(Car car, TrackReader trackReader, int trackPieceCount)
    {
      _trackReader = trackReader;
      Car = car;
      PieceInfo = Enumerable.Range(0, trackPieceCount).Select(i => new PositionAnalyzerCarPieceInfo()).ToArray();
      Power = 5;
      Drag = 0.02d;
      FCentrifugalSlip = 0.3231d;
    }


    private bool _powerCalculated = false;
    private bool _dragCalculated = false;
    private bool _massCalculated = false;
    private bool _fCentrifugalSlipCalculated = false;
    public void Add(PositionAnalyzerCarTick tick)
    {
      _ticks.Add(tick);
      Update();

      if (!IsSelf)
        return;

      if (!_powerCalculated && tick.GameTick == 1)
      {
        // Power = Throttle/Vnext (by Tick0 stel Drag == 0)
        Power = tick.PreviousTick.Throttle / tick.Speed;
        _powerCalculated = true;
        System.Console.WriteLine("Power: {0}", Power);
      }

      if (!_dragCalculated && tick.GameTick == 2)
      {
        // Drag = 1 + (Throttle/Power - Vnext)/V0
        Drag = 1 + (tick.PreviousTick.Throttle / Power - tick.Speed) / tick.PreviousTick.Speed;
        _dragCalculated = true;
        System.Console.WriteLine("Drag: {0}", Drag);
        System.Console.WriteLine("Vmax: {0}", tick.PreviousTick.Throttle / Power / Drag);

        // Alternative drag (=power*drag) for use in mass calc
        var v0 = tick.PreviousTick.PreviousTick.Speed;
        var v1 = tick.PreviousTick.Speed;
        var v2 = tick.Speed;

        System.Console.WriteLine("V0: {0}", v0);
        System.Console.WriteLine("V1: {0}", v1);
        System.Console.WriteLine("V2: {0}", v2);

        Drag2 = (v1 - (v2 - v1))/(v1*v1);//*tick.Throttle;

        System.Console.WriteLine("Drag2: {0}", Drag2);
        System.Console.WriteLine("Vmax2: {0}", 1 / Drag2);
      }

      if (!_massCalculated && tick.GameTick == 3)
      {
        // m = 1.0 / ( ln( ( v(3) - ( h / k ) ) / ( v(2) - ( h / k ) ) ) / ( -k ) )
        var throttle = tick.PreviousTick.Throttle;//1.0;
        //var v0 = tick.PreviousTick.PreviousTick.PreviousTick.Speed;
        //var v1 = tick.PreviousTick.PreviousTick.Speed;
        var v2 = tick.PreviousTick.Speed;
        var v3 = tick.Speed;

        // Mass = 1.0d / ( ln( ( v3 - ( throttle / drag ) ) / ( v2 - ( throttle / drag ) ) ) / ( -drag ) )
        Mass = 1.0d / (Math.Log((v3 - (throttle / Drag2)) / (v2 - (throttle / Drag2))) / (-Drag2));

        _massCalculated = true;
        System.Console.WriteLine("Mass: {0}", Mass);
      }

      if (!_fCentrifugalSlipCalculated && tick.Angle != 0)
      {
        var v = tick.Speed;
        var angle = tick.Angle;
        var r = _trackReader[tick.RacePieceIndex].Lanes[tick.LaneStart].Radius;

        var angleVelocityInDegrees = RadianToDegree(v/r);

        var angleVelocitySlip = angleVelocityInDegrees - angle;

        var vThres = DegreeToRadian(angleVelocitySlip)*r;

        var fCentSlip = vThres*vThres/r;

        FCentrifugalSlip = fCentSlip;

        _fCentrifugalSlipCalculated = true;
        System.Console.WriteLine("FCentSpeed: {0}", v);
        System.Console.WriteLine("FCentSlip: {0}", FCentrifugalSlip);
      }
    }

    private double RadianToDegree(double input)
    {
      return input/2/Math.PI*360;
    }

    private double DegreeToRadian(double input)
    {
      return input*2*Math.PI/360;
    }

    private void Update()
    {
      var tickCount = _ticks.Count;

      var last = _ticks[tickCount - 1];
      LastTick = last;

      //RacePieceIndex = last.RacePieceIndex;
      //Angle = last.Angle;

      var prev = tickCount >= 2 ? _ticks[tickCount - 2] : null;
      if (prev == null)
        return;

      var travelled = GetTravelled(last, prev);
      last.Speed = travelled;
      //Speed = travelled;//60; // m/s

      var prev2 = tickCount >= 3 ? _ticks[tickCount - 3] : null;
      if (prev2 == null)
        return;

      var acceleration = GetAcceleration(last, prev);
      last.Acceleration = acceleration;
      //Acceleration = acceleration;

      var angleSpeed = GetAngleSpeed(last, prev);
      last.AngleSpeed = angleSpeed;

      var angleAcceleration = GetAngleAcceleration(last, prev);
      last.AngleAcceleration = angleAcceleration;

      //if (!AreAlmostEqual(last.Speed, prev.SpeedNextExpected))
      //{
      //  last.MaybeBumpedOrSwitch = true;
      //  last.ProbablyBumped = true;
        
      //  // TODO: calc switch distance => now unable to detect bump on last ticks in switch
      //  if (prev2.LaneStart != last.LaneEnd)
      //    last.ProbablyBumped = false;

      //  if (last.ProbablyBumped)
      //  {
      //    Console.WriteLine("Bumped {0} {1} {2}?", last.GameTick, last.Speed, prev.SpeedNextExpected);
      //    //Console.WriteLine(string.Join(",", new[]{last, prev, prev2}.Select(t => string.Format(@"[{0},{1}]", t.LaneStart, t.LaneEnd))));
      //  }
      //}
    }

    private bool AreAlmostEqual(double value1, double value2)
    {
      var rounded1 = Math.Round(value1, 5);
      var rounded2 = Math.Round(value2, 5);
      return rounded1 == rounded2;
    }

    private double GetAcceleration(PositionAnalyzerCarTick current, PositionAnalyzerCarTick prev)
    {
      return current.Speed - prev.Speed;


      if (current.Speed == 0)
        return 0;

      var currentSpeed2 = current.Speed * current.Speed;
      var prevSpeed2 = prev.Speed * prev.Speed;
      return (currentSpeed2 - prevSpeed2) / (2 * current.Speed);
    }

    private double GetAngleSpeed(PositionAnalyzerCarTick current, PositionAnalyzerCarTick prev)
    {
      return current.Angle - prev.Angle;
    }

    private double GetAngleAcceleration(PositionAnalyzerCarTick current, PositionAnalyzerCarTick prev)
    {
      return current.AngleSpeed - prev.AngleSpeed;

      if (current.AngleSpeed == 0)
        return 0;

      var currentAngleSpeed2 = current.AngleSpeed * current.AngleSpeed * Math.Sign(current.AngleSpeed);
      var prevAngleSpeed2 = prev.AngleSpeed * prev.AngleSpeed * Math.Sign(current.AngleSpeed);
      
      return Math.Abs(currentAngleSpeed2 - prevAngleSpeed2) / (2 * current.AngleSpeed);
    }

    public TrackReader.RelativeTrackReader GetTrackReader()
    {
      return new TrackReader.RelativeTrackReader(_trackReader, LastTick.RacePieceIndex);
    }


    private double GetTravelled(PositionAnalyzerCarTick piece1, PositionAnalyzerCarTick piece2)
    {
      var last = piece1;
      var prev = piece2;

      if (last.RacePieceIndex < prev.RacePieceIndex)
      {
        var temp = last;
        last = prev;
        prev = temp;
      }

      // In piece
      if (last.RacePieceIndex == prev.RacePieceIndex)
      {
        return last.InPieceDistance - prev.InPieceDistance;
      }

      // Distance travelled in prev piece
      var result = _trackReader[prev.RacePieceIndex][prev.LaneStart, prev.LaneEnd].Length - prev.InPieceDistance;

      // Distance travelled intermediate pieces (without lane information...)
      for (int i = prev.RacePieceIndex + 1; i < last.RacePieceIndex; i++)
        result += _trackReader[i].Length;

      // Distance travelled in last piece
      result += last.InPieceDistance;

      return result;
    }


    public double CalculateThrottleForSpeed(double targetSpeed)
    {
      if (LastTick == null || LastTick.GameTick <= 2)
        return 1;

      // Vnext = V0 + Throttle/Power - V0*Drag

      // Power = Throttle/Vnext (by Tick0 stel Drag == 0)
      // Drag = (V0 + Throttle/Power - Vnext)/V0
      // Drag = 1 + (Throttle/Power - Vnext)/V0

      //tickInfo.SpeedNextExpected = tickInfo.Speed + throttle / Analyzer.Self.Power - tickInfo.Speed * Analyzer.Self.Drag;

      //throttle = (Vnext - V0 + V0*Drag)*power

      var lastTick = LastTick;

      var throttle = (targetSpeed - lastTick.Speed + (lastTick.Speed * Drag)) * Power;

      //if (throttle > 1)
      //  throttle = 1;
      //if (throttle < 0)
      //  throttle = 0;

      return throttle;
    }

    public double CalculateSpeedForThrottle(double throttle)
    {
      if (LastTick == null || LastTick.GameTick <= 2)
        return double.MaxValue;

      var lastTick = LastTick;

      var speed = throttle / Power + lastTick.Speed + (lastTick.Speed * Drag);
      return speed;
    }
  }

  public class PositionAnalyzerCarTick
  {
    public PositionAnalyzerCarTick PreviousTick { get; set; }

    public int Lap { get; set; }
    public int PieceIndex { get; set; }

    public int RacePieceIndex { get; set; }

    public int LaneStart { get; set; }
    public int LaneEnd { get; set; }
    public double InPieceDistance { get; set; }

    public double Angle { get; set; }

    public int GameTick { get; set; }


    // Set
    public double Throttle { get; set; }

    // Calculated
    public double Speed { get; set; }
    public double Acceleration { get; set; }
    public double AngleSpeed { get; set; }
    public double AngleAcceleration { get; set; }

    public double LogInfo1 { get; set; }

    // TODO
    public bool Crashed { get; set; }

    public bool TurboAvailable { get; set; }
    public TurboAvailableMessage.TurboAvailableData TurboAvailableData { get; set; }

    public bool TurboActive { get; set; }
    public double TurboActiveFactor { get; set; }
    public TurboAvailableMessage.TurboAvailableData TurboActiveData { get; set; }
    public int TurboActiveTickRemaining { get; set; }

    public PositionAnalyzerCarTick()
    {
      TurboActiveFactor = 1d;
    }

    public void UpdateSendThrottle()
    {
      if (PreviousTick == null)
        return;
      Throttle = PreviousTick.Throttle;
    }

    public void UpdateSendThrottle(double throttle)
    {
      Throttle = throttle;
    }

    public double SpeedNextExpected { get; set; }
    public bool MaybeBumpedOrSwitch { get; set; }
    public bool ProbablyBumped { get; set; }
    // Debug ivm zoeken formula
    public double AngleNextExpected { get; set; }
  }

  public class PositionAnalyzerCarPieceInfo
  {
    public int PieceIndex { get; set; }
    public double LastExitSpeed { get; set; }

    public PositionAnalyzerCarPieceInfo()
    {
      LastExitSpeed = 3;
    }
  }

}

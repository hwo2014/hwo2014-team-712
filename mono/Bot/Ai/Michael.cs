﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Bot.Ai.BotV2;
using Bot.Connection;
using Bot.Messages;

namespace Bot.Ai
{
  public class Michael : BotV2Base
  {
    public Michael(IConnection connection, BaseMessage joinMessage) : base(connection, joinMessage)
    {
    }


    protected override GameTickResponse CalculateGameTickResponse(BaseMessage message)
    {
      var result = base.CalculateGameTickResponse(message);
      var tickInfo = Analyzer.Self.LastTick;

      var throttle5 = Analyzer.Self.CalculateThrottleForSpeed(5d);
      var throttle7 = Analyzer.Self.CalculateThrottleForSpeed(7d);
      var throttle8 = Analyzer.Self.CalculateThrottleForSpeed(8d);
      var priority = GameTickResponsePriority.Optimal;

      var trackReader = Analyzer.Self.GetTrackReader();

      var throttle = throttle7;
      //if (tickInfo.PieceIndex > 4 || (tickInfo.PieceIndex == 4 && tickInfo.InPieceDistance >= 0))
      //  throttle = 0;

      //if (tickInfo.GameTick >= 60 && tickInfo.GameTick <= 62)
      //  throttle = 0;
      //if (tickInfo.GameTick == 63)
      //  throttle = 0.331;

      //if (tickInfo.GameTick >= 82)
      //  throttle = 0;

      //if (tickInfo.GameTick >= 112)
      //  throttle = throttle8;



      result.Options.Add(new GameTickResponse.Option()
      {
        Priority = priority,
        Message = GameTickResponseMessage.Throttle,
        Throttle = throttle
      });

      //if (tickInfo.PieceIndex >= 4)
      //  Thread.Sleep(250);

      if (tickInfo.GameTick >= 75 || tickInfo.PieceIndex >= 4)
      {
        Console.WriteLine("{0}\t{1:00.000000}\t{2:000.000000}\t{3:00.000000}\t{4:00.000000}\t{5:00.000000}\t{6}\t{7:000.000000}\t{8:000.000000}\t{9:000.000000}",
          tickInfo.GameTick,
          tickInfo.Speed,
          tickInfo.Acceleration,
          tickInfo.Angle,
          tickInfo.AngleSpeed,
          tickInfo.AngleAcceleration,
          tickInfo.PieceIndex,
          trackReader[0].Lanes[tickInfo.LaneStart].Radius,
          tickInfo.InPieceDistance,
          trackReader[0].Lanes[tickInfo.LaneStart].Length);
      }

      if (tickInfo.PieceIndex >= 11)
      {
        // TODO remove code in MessageReader - error and basebot send message
        result.Options.Add(new GameTickResponse.Option()
        {
          Priority = GameTickResponsePriority.Override,
          Message = GameTickResponseMessage.Stop,
        });
      }

      return result;
    }


  }
}

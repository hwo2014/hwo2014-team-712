﻿using System;
using System.Reactive.Linq;
using Bot.Connection;
using Bot.Messages;

namespace Bot.Ai
{
  public abstract class BotAnalyzerBase : BotBase, IBotAnalyzerBase
  {

    private PositionAnalyzer _analyzer;
    public PositionAnalyzer Analyzer
    {
      get
      {
        if (_analyzer == null)
          _analyzer = new PositionAnalyzer(Race, BotColor);
        return _analyzer;
      }
    }

    protected BotAnalyzerBase(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {
    }

    protected override void Setup(IObservable<BaseMessage> messages)
    {
      messages.OfType<GameStartMessage>();

      var gameEndSubscription = messages.OfType<GameEndMessage>().Subscribe(HandleGameEnd);
      _disposables.Add(gameEndSubscription);

      var carPositionsSubscription = messages.OfType<CarPositionsMessage>().Subscribe(HandleCarPositions);
      _disposables.Add(carPositionsSubscription);
    }

    private void HandleGameEnd(GameEndMessage message)
    {
      Analyzer.Reset();
    }

    private void HandleCarPositions(CarPositionsMessage message)
    {
      Analyzer.Update(message);

      HandleGameTick(message);
    }

    protected abstract void HandleGameTick(CarPositionsMessage message);


  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Bot.Ai.BotV2;
using Bot.Connection;
using Bot.Messages;
using Bot.Models;

namespace Bot.Ai
{
  public class StaticKeimola : BotV2Base
  {
    public StaticKeimola(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {
    }

    protected override void HandleGameInit(GameInitMessage message)
    {
      base.HandleGameInit(message);


      foreach (var piece in Race.Track.Pieces)
      {
        foreach (var lane in piece.Lanes)
        {

          if (lane.Radius == 110)
          {
            lane.TempEntrySpeed = 7.2d;
            lane.TempMaxSpeed = 9d;
          }
          else if (lane.Radius == 90)
          {
            lane.TempEntrySpeed = 6.5d;
            lane.TempMaxSpeed = 8d;
          }
          else if (lane.Radius == 190)
          {
            lane.TempEntrySpeed = 16.5d;
            lane.TempMaxSpeed = 18d;
          }
          else if (lane.Radius == 210)
          {
            lane.TempEntrySpeed = 16.5d;
            lane.TempMaxSpeed = 18d;
          }
          else
          {
            var maxCentriForce = 0.32 + 0.1;
            lane.TempEntrySpeed = Math.Sqrt(maxCentriForce * lane.Radius);

            maxCentriForce = 0.32 + 0.2;
            lane.TempMaxSpeed = Math.Sqrt(maxCentriForce * lane.Radius);
          }

          if (piece.Index >= 31 && piece.Index <= 34)
          {
            lane.TempEntrySpeed -= 0.1;
            lane.TempMaxSpeed -= 0.0;
          }
          if (piece.Index >= 26 && piece.Index <= 27)
          {
            lane.TempEntrySpeed -= 0.1;
            lane.TempMaxSpeed -= 0.0;
          }

        }
      }
    }

    protected override GameTickResponse CalculateGameTickResponse(BaseMessage message)
    {
      var result = base.CalculateGameTickResponse(message);
      var tickInfo = Analyzer.Self.LastTick;
      var trackReader = Analyzer.Self.GetTrackReader();

      var maxSpeed = double.MaxValue;

      var currentPiece = trackReader[0];
      var currentLane = currentPiece.Lanes[tickInfo.LaneStart];

      if (currentPiece.Way != Way.Straight)
      {
        var maxCornerSpeed = currentLane.TempEntrySpeed;
        if ((tickInfo.AngleSpeed * (int)currentPiece.Way) < 0)
        {
          maxCornerSpeed = currentLane.TempMaxSpeed;
        }

        if (maxCornerSpeed < maxSpeed)
          maxSpeed = maxCornerSpeed;
      }

      for (int i = 0; i < 5; i++)
      {
        var piece = GetNextCorner(trackReader, i);
        if (AreEqual(currentPiece, piece))
          continue;

        var maxCornerSpeed = GetMaxSpeedForRelativeCorner(trackReader, tickInfo, i);

        if (maxCornerSpeed < maxSpeed)
          maxSpeed = maxCornerSpeed;

      }




      if (maxSpeed > 100)
        maxSpeed = 100;
      tickInfo.LogInfo1 = maxSpeed;
      var throttle = Analyzer.Self.CalculateThrottleForSpeed(maxSpeed);
      var priority = GameTickResponsePriority.Optimal;
      result.Options.Add(new GameTickResponse.Option()
      {
        Priority = priority,
        Message = GameTickResponseMessage.Throttle,
        Throttle = throttle
      });

      if (tickInfo.GameTick == 10)
      {
        result.Options.Add(new GameTickResponse.Option()
        {
          Priority = GameTickResponsePriority.Override,
          Message = GameTickResponseMessage.Switch,
          SwitchLane = SwitchLane.Right
        });
      }

      if (tickInfo.TurboAvailable && tickInfo.PieceIndex == 35 && !turboSend)
      {
        result.Options.Add(new GameTickResponse.Option()
        {
          Priority = GameTickResponsePriority.Override,
          Message = GameTickResponseMessage.Turbo
        });
        turboSend = true;
      }
      else
        turboSend = false;

      return result;
    }

    private bool turboSend;

    private bool AreEqual(TrackPiece piece1, TrackPiece piece2)
    {
      if (piece1.Way != piece2.Way)
        return false;
      if (piece1.Radius != piece2.Radius)
        return false;

      return true;
    }


    private double GetMaxSpeedForRelativeCorner(TrackReader.RelativeTrackReader trackReader, PositionAnalyzerCarTick tickInfo, int corner)
    {
      var cornerPiece = GetNextCorner(trackReader, corner);

      var distanceToCorner = GetDistanceToPiece(trackReader, tickInfo.InPieceDistance, tickInfo.LaneStart, cornerPiece);

      double maxSpeed = cornerPiece.Lanes[tickInfo.LaneStart].TempEntrySpeed;

      if (distanceToCorner <= 0)
        return Analyzer.Self.CalculateThrottleForSpeed(maxSpeed);

      var ticksToSlowDownToMaxSpeed = Math.Log(maxSpeed / tickInfo.Speed, 1 - Analyzer.Self.Drag);

      var drag = Analyzer.Self.Drag;
      var eenMinDrag = 1 - drag;
      var remWeg = tickInfo.Speed * (eenMinDrag - Math.Pow(eenMinDrag, ticksToSlowDownToMaxSpeed + 1)) / drag;

      if (remWeg >= distanceToCorner)
        return 0;

      // remWegInTicks > distanceToCorner
      var afstandOver = distanceToCorner - remWeg;
      var speed = tickInfo.Speed;

      var bigPart = (afstandOver + speed * eenMinDrag / drag) / (1 + eenMinDrag / drag);
      var maxThrottleForOneTickAndStillBeBrakableInDistance =
        Analyzer.Self.Power * (bigPart - speed * eenMinDrag);

      return Analyzer.Self.CalculateSpeedForThrottle(maxThrottleForOneTickAndStillBeBrakableInDistance);
    }

    private TrackPiece GetNextCorner(TrackReader.RelativeTrackReader trackReader, int corner)
    {
      var count = -1;
      for (int i = 0; ; i++)
      {
        var result = trackReader[i];
        if (result.Way == Way.Straight)
          continue;

        count++;
        if (count >= corner)
          return result;
      }
    }



    private double GetDistanceToPiece(TrackReader.RelativeTrackReader trackReader, double inPieceDistance, int lane, TrackPiece piece)
    {
      var distance = 0d;

      var currentPiece = trackReader[0];
      if (piece.Index == currentPiece.Index)
        return distance;

      distance = currentPiece.Lanes[lane].Length - inPieceDistance;
      for (int i = 1; ; i++)
      {
        currentPiece = trackReader[i];
        if (piece.Index == currentPiece.Index)
          return distance;

        distance += currentPiece.Lanes[lane].Length;
      }
    }


  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reactive.Linq;
using Bot.Connection;
using Bot.Messages;
using Bot.Models;

namespace Bot.Ai
{
  public class BotTurbo1 : BotAnalyzerBase
  {
    private StraightLength[] _startPieceIndexes = new StraightLength[0];
    private TurboAvailableMessage.TurboAvailableData _turboAvailable;

    public class StraightLength
    {
      public int PieceIndex { get; set; }
      public int Pieces { get; set; }
      public double Length { get; set; }
    }

    public BotTurbo1(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {
    }

    protected override void Setup(IObservable<BaseMessage> messages)
    {
      base.Setup(messages);

      var turboAvailableSubscription = messages.OfType<TurboAvailableMessage>().Subscribe(HandleTurboAvailable);
      _disposables.Add(turboAvailableSubscription);
    }

    protected override void HandleGameInit(GameInitMessage message)
    {
      base.HandleGameInit(message);

      if (Race.Track.Pieces.Any(p => p.Way == Way.Straight))
        _startPieceIndexes = SearchStraightPieces().ToArray();
    }

    private void HandleTurboAvailable(TurboAvailableMessage message)
    {
      _turboAvailable = message.Data;
    }

    private IEnumerable<StraightLength> SearchStraightPieces()
    {
      var track = Race.Track;

      StraightLength straightLength = null;
      for (int i = 0; i < track.Pieces.Length; i++)
      {
        var piece = track.Pieces[i];
        if (piece.Way == Way.Straight)
        {
          if (straightLength == null)
            straightLength = new StraightLength() { PieceIndex = i };

          straightLength.Pieces += 1;
          straightLength.Length += piece.Length;
          continue;
        }

        if (straightLength == null)
          continue;

        // Skip first straight if last piece is straight
        if (straightLength.PieceIndex != 0 || track.Pieces.Last().Way != Way.Straight)
          yield return straightLength;

        straightLength = null;
      }

      if (straightLength == null)
        yield break;

      foreach (var piece in track.Pieces)
      {
        if (piece.Way != Way.Straight)
          break;

        straightLength.Pieces += 1;
        straightLength.Length += piece.Length;
      }

      yield return straightLength;
    }

    protected override void HandleGameTick(CarPositionsMessage message)
    {
      var throttle = 1d;

      var carAnalyzer = Analyzer[BotColor];

      var trackReader = carAnalyzer.GetTrackReader();

      var currentSpeed = carAnalyzer.LastTick.Speed;
      var currentAcceleration = carAnalyzer.LastTick.Acceleration;

      var targetSpeed = int.MaxValue;
      var nextCornerTargetSpeed = 1m;

      var distanceToCorner = GetDistanceToCorner(trackReader, carAnalyzer.LastTick.InPieceDistance);


      if (trackReader[0].Way != Way.Straight)
      { //in bocht 
        throttle = 0.3d;
      }
      else 
      { //recht

        if (_turboAvailable != null)
        {
          var longestStraights = _startPieceIndexes.GroupBy(p => p.Length).OrderByDescending(p => p.Key).FirstOrDefault();
          if (longestStraights != null && longestStraights.Any(p => p.PieceIndex == trackReader.CurrentPiece))
          {
            SendMessage(new TurboMessage()
            {
              Data = "Till next time...",
              GameTick = message.GameTick
            });
            _turboAvailable = null;
            return;
          }
        }

        if (currentSpeed > 4) 
          if (distanceToCorner < 20)
            throttle = 0.0d;
      }
      
      SendMessage(new ThrottleMessage()
      {
        Data = throttle,
        GameTick = message.GameTick
      });
    }

    private double GetDistanceToCorner(TrackReader.RelativeTrackReader trackReader, double inPieceDistance)
    {
      var currentPiece = trackReader[0];
      if (currentPiece.Way != Way.Straight)
        return 0d;

      var result = currentPiece.Length - inPieceDistance;
      
      var i = 1;
      while (trackReader[i].Way != Way.Straight 
          && i != currentPiece.Index)
      {
        result += trackReader[i].Length;
        i++;
      }
      return result;
    }
  }
}

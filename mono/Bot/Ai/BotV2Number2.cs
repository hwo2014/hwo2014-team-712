﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Bot.Ai.BotV2;
using Bot.Connection;
using Bot.Messages;
using Bot.Models;

namespace Bot.Ai
{
  public class BotV2Number2 : BotV2Base
  {
    private OptimalPlacesForTurboAnalyzer _optimalPlacesForTurboAnalyzer;
    private MaxSpeedForCornerAnalyzer _maxSpeedForCornerAnalyzer;


    public BotV2Number2(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {

    }

    protected override void HandleGameInit(GameInitMessage message)
    {
      base.HandleGameInit(message);

      _optimalPlacesForTurboAnalyzer = new OptimalPlacesForTurboAnalyzer(Race);
      _maxSpeedForCornerAnalyzer = new MaxSpeedForCornerAnalyzer(Race, Analyzer.Self);
    }


    protected override GameTickResponse CalculateGameTickResponse(BaseMessage message)
    {
      var result = base.CalculateGameTickResponse(message);
      var tickInfo = Analyzer.Self.LastTick;

      var throttle = double.MaxValue;//Analyzer.Self.CalculateThrottleForSpeed(5d);
      var priority = GameTickResponsePriority.Optimal;

      var trackReader = Analyzer.Self.GetTrackReader();

      for (int corner = 0; corner < 5; corner++)
      {
        var maxThrottleForCorner = GetMaxThrottleForRelativeCorner(trackReader, tickInfo, corner);
        if (corner == 0 && trackReader[0].Way != Way.Straight)
          tickInfo.LogInfo1 = _logMaxSpeedInGetMaxThrottleForRelativeCorner;

        if (maxThrottleForCorner < throttle)
          throttle = maxThrottleForCorner;

        if (throttle < tickInfo.TurboActiveFactor)
        {
          priority = GameTickResponsePriority.NeededToMakeCorner;
        }
      }

      result.Options.Add(new GameTickResponse.Option()
      {
        Priority = priority,
        Message = GameTickResponseMessage.Throttle,
        Throttle = throttle
      });

      if (tickInfo.TurboAvailable 
        && !tickInfo.TurboActive 
        && _optimalPlacesForTurboAnalyzer != null 
        && _optimalPlacesForTurboAnalyzer.IsOptimal(tickInfo))
      {
        result.Options.Add(new GameTickResponse.Option()
        {
          Priority = GameTickResponsePriority.TurboIfPossible,
          Message = GameTickResponseMessage.Turbo,
          TurboMessage = @"What does this button ... vroom vroom"
        });
        //Log("Request turbo");
      }


      // Basic lane switching
      if (Analyzer.Self.FCentrifugalSlipCalculated)
      {
        var nextPiece = trackReader[1];
        if (nextPiece.Switch)
        {
          var shortestLane = nextPiece.Lanes.OrderBy(l => l.DistanceToNextSwitch).First();

          // Disable optimal lane, to have simple take over possibilities
          //var nextShortestLane = nextPiece.Lanes.OrderBy(l => l.DistanceToNextSwitch).Skip(1).First();
          //if (shortestLane.DistanceToNextSwitch == nextShortestLane.DistanceToNextSwitch &&
          //    tickInfo.LaneEnd == nextShortestLane.Index)
          //  shortestLane = nextShortestLane;
          
          if (tickInfo.LaneEnd != shortestLane.Index)
          {
            var switchTo = SwitchLane.Right;

            if (tickInfo.LaneEnd > shortestLane.Index)
              switchTo = SwitchLane.Left;

            if (switchTo != SwitchLaneCommandSendToServer)
            {
              result.Options.Add(new GameTickResponse.Option()
              {
                Priority = GameTickResponsePriority.SwitchLane,
                Message = GameTickResponseMessage.Switch,
                SwitchLane = switchTo
              });
            }
          }
        }
      }




      // TODO: Move to basebot...
      tickInfo.SpeedNextExpected = tickInfo.Speed + GetClippedThrottle(result, throttle) / Analyzer.Self.Power - tickInfo.Speed * Analyzer.Self.Drag;
      //tickInfo.AngleNextExpected = tickInfo.Speed + throttle / Analyzer.Self.Power - tickInfo.Speed * Analyzer.Self.Drag;



      //if (tickInfo.PieceIndex == 6)
      //  LogLine(@"Tick: {0}, Angle:{1}, In:{2}, Len:{3}, Ra:{4}", 
      //    tickInfo.GameTick, 
      //    tickInfo.Angle,
      //    tickInfo.InPieceDistance, 
      //    Analyzer.Self.GetTrackReader()[0][tickInfo.LaneStart, tickInfo.LaneEnd].Length,
      //    Analyzer.Self.GetTrackReader()[0][tickInfo.LaneStart, tickInfo.LaneEnd].Radius);

      //if (tickInfo.PieceIndex >= 1 && tickInfo.PieceIndex <= 8)
      //  LogLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}",
      //    tickInfo.GameTick,
      //    maxSpeed,
      //    nextCorner.Radius,
      //    distanceToCorner,
      //    ticksToSlowDownToMaxSpeed,
      //    //tickInfo.Angle,
      //    //tickInfo.AngleNextExpected,
      //    //tickInfo.AngleSpeed,
      //    //tickInfo.AngleAcceleration,
      //    tickInfo.Speed);

      //Thread.Sleep(250);

      //var expectedSpeed = 0d;
      //if (tickInfo.PreviousTick != null)
      //  expectedSpeed = tickInfo.PreviousTick.SpeedNextExpected;
      //if (tickInfo.InPieceDistance < 12 && tickInfo.PieceIndex == 2)
      //  LogLine(@"Tick: {0}, Speed: {1}, Exp:{2}, Acc: {3}", tickInfo.GameTick, tickInfo.Speed, expectedSpeed, tickInfo.Acceleration);

      return result;
    }

    private double _logMaxSpeedInGetMaxThrottleForRelativeCorner;

    private double GetMaxThrottleForRelativeCorner(TrackReader.RelativeTrackReader trackReader, PositionAnalyzerCarTick tickInfo, int corner)
    {
      var cornerPiece = GetNextCorner(trackReader, corner);

      var distanceToCorner = GetDistanceToPiece(trackReader, tickInfo.InPieceDistance, tickInfo.LaneStart, cornerPiece);

      // centriForce = v^2/r
      //var self = Analyzer.Self;
      //var maxCentriForce = self.FCentrifugalSlip;//  0.32 + 0.1;
      //if (!self.FCentrifugalSlipCalculated)
      //  maxCentriForce += 1 + self.LastTick.Lap; // Force some slip
      //else
      //{
      //  //maxCentriForce = maxCentriForce+0.1;
      //}
      double maxSpeed;
      //if (_maxSpeedForCornerAnalyzer != null && _maxSpeedForCornerAnalyzer.Car != null)
      //  maxSpeed = _maxSpeedForCornerAnalyzer[cornerPiece, cornerPiece.Lanes[tickInfo.LaneStart]];
      //else
      {
        var maxCentriForce = 0.32 + 0.1;
        if (Analyzer.Self.FCentrifugalSlipCalculated && Analyzer.Self.FCentrifugalSlip < maxCentriForce)
          maxCentriForce = Analyzer.Self.FCentrifugalSlip;
        maxSpeed = Math.Sqrt(maxCentriForce * cornerPiece.Lanes[tickInfo.LaneStart].Radius);
      }
      _logMaxSpeedInGetMaxThrottleForRelativeCorner = maxSpeed;

      if (distanceToCorner <= 0)
        return Analyzer.Self.CalculateThrottleForSpeed(maxSpeed);
      
      var ticksToSlowDownToMaxSpeed = Math.Log(maxSpeed / tickInfo.Speed, 1 - Analyzer.Self.Drag);

      var drag = Analyzer.Self.Drag;
      var eenMinDrag = 1 - drag;
      var remWeg = tickInfo.Speed * (eenMinDrag - Math.Pow(eenMinDrag, ticksToSlowDownToMaxSpeed + 1)) / drag;

      if (remWeg >= distanceToCorner)
        return 0;

      // remWegInTicks > distanceToCorner
      var afstandOver = distanceToCorner - remWeg;
      var speed = tickInfo.Speed;

      var bigPart = (afstandOver + speed*eenMinDrag/drag) / (1 + eenMinDrag/drag);
      var maxThrottleForOneTickAndStillBeBrakableInDistance =
        Analyzer.Self.Power * (bigPart - speed * eenMinDrag);

      return maxThrottleForOneTickAndStillBeBrakableInDistance;
    }

    private TrackPiece GetNextCorner(TrackReader.RelativeTrackReader trackReader, int corner)
    {
      var count = -1;
      for (int i = 0; ; i++)
      {
        var result = trackReader[i];
        if (result.Way == Way.Straight)
          continue;

        count ++;
        if (count >= corner)
          return result;
      }
    }


    private double GetDistanceToPiece(TrackReader.RelativeTrackReader trackReader, double inPieceDistance, int lane, TrackPiece piece)
    {
      var distance = 0d;

      var currentPiece = trackReader[0];
      if (piece.Index == currentPiece.Index)
        return distance;

      distance = currentPiece.Lanes[lane].Length - inPieceDistance;
      for (int i = 1; ; i++)
      {
        currentPiece = trackReader[i];
        if (piece.Index == currentPiece.Index)
          return distance;

        distance += currentPiece.Lanes[lane].Length;
      }
    }


  }
}

﻿using System;
using System.Linq;
using System.Reactive.Linq;
using Bot.Connection;
using Bot.Messages;

namespace Bot.Ai
{
  public class Bot1 : BotBase
  {
    public Bot1(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {
    }

    protected override void Setup(IObservable<BaseMessage> messages)
    {
      messages.OfType<GameStartMessage>();

      var carPositionsSubscription = messages.OfType<CarPositionsMessage>().Subscribe(HandleCarPositions);
      _disposables.Add(carPositionsSubscription);
    }

    private void HandleCarPositions(CarPositionsMessage message)
    {
      var speed = 1d;

      var carInfo = message.Data.First(c => c.Id.Color == BotColor);

      if (Math.Abs(carInfo.Angle) > 10)
        speed = 0;
      else
      {
        var tr = new TrackReader(Race.Track);
        var rtr = new TrackReader.RelativeTrackReader(tr, carInfo.PiecePosition.Lap, carInfo.PiecePosition.PieceIndex);

        if (rtr[1].Angle != 0)
          speed = 0.5d;
      }

      //var ticky = message.GameTick%250;
      //if (ticky < 10)
      //  speed = 0m;
      //else if (ticky < 50)
      //  speed = 0.1m;

      SendMessage(new ThrottleMessage()
      {
        Data = speed
      });
    }



  }
}

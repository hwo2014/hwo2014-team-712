﻿using System;
using System.Collections.Generic;
using Bot.Connection;
using Bot.Legacy;
using Bot.Messages;
using Newtonsoft.Json;

namespace Bot.Ai
{
  class MessageReader
  {
    private readonly IConnection _connection;

    public MessageReader(IConnection connection)
    {
      _connection = connection;
    }

    public IEnumerable<BaseMessage> ObservableMessages()
    {
      foreach (var line in _connection.ReadLines())
      {
        var msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
        switch (msg.msgType)
        {
          case "carPositions":
            yield return JsonConvert.DeserializeObject<CarPositionsMessage>(line);
            //send(new Throttle(0.65));
            break;

          case "join":
          case "createRace":
          case "joinRace":
            LogOnNewLine("Joined");
            yield return JsonConvert.DeserializeObject<JoinMessage>(line);
            //send(new Ping());
            break;

          case "yourCar":
            var yourCar = JsonConvert.DeserializeObject<YourCarMessage>(line);
            LogOnNewLine(@"Driving in {0}", yourCar.Data.Color);
            yield return yourCar;
            break;

          case "crash":
            LogOnNewLine("Crash");
            yield return JsonConvert.DeserializeObject<CrashMessage>(line);
            break;

          case "spawn":
            LogOnNewLine("Spawn");
            yield return JsonConvert.DeserializeObject<SpawnMessage>(line);
            break;

          case "lapFinished":
            //Console.WriteLine("Lap completed");
            var lapFinished = JsonConvert.DeserializeObject<LapFinishedMessage>(line);
            LogOnNewLine(@"Lap completed by {0} in {1:mm\:ss\.fff}.", lapFinished.Data.Car.Color, lapFinished.Data.LapTime.Time);
            yield return lapFinished;
            break;

          case "turboAvailable":
            LogOnNewLine("TurboAvailable");
            yield return JsonConvert.DeserializeObject<TurboAvailableMessage>(line);
            break;

          case "turboStart":
            LogOnNewLine("TurboStart");
            yield return JsonConvert.DeserializeObject<TurboStartMessage>(line);
            break;

          case "turboEnd":
            LogOnNewLine("TurboEnd");
            yield return JsonConvert.DeserializeObject<TurboEndMessage>(line);
            break;

          case "gameInit":
            LogOnNewLine("Race init");
            //LogOnNewLine("================================");
            //LogOnNewLine(line);
            //LogOnNewLine("================================");
            yield return JsonConvert.DeserializeObject<GameInitMessage>(line);
            //send(new Ping());
            break;

          case "gameStart":
            LogOnNewLine("Race starts");
            yield return JsonConvert.DeserializeObject<GameStartMessage>(line);
            //send(new Ping());
            break;

          case "gameEnd":
            LogOnNewLine("Race ended");
            yield return JsonConvert.DeserializeObject<GameEndMessage>(line);
            //send(new Ping());
            break;

          case "tournamentEnd":
            LogOnNewLine("Tournament ended");
            yield return JsonConvert.DeserializeObject<TournamentEndMessage>(line);
            //send(new Ping());
            break;

          case "finish":
            LogOnNewLine("Finished");
            yield return JsonConvert.DeserializeObject<FinishMessage>(line);
            //send(new Ping());
            break;

          case "error":
            LogOnNewLine("Error");
            var error = JsonConvert.DeserializeObject<ErrorMessage>(line);
            // TODO: TEMP to force disconnect... !!!
            if (error.Data != @"Invalid throttle value")
              throw new Exception(error.Data);
            yield return error; 
            //send(new Ping());
            break;

          default:
            LogOnNewLine(@"Unknown message: {0}", msg.msgType);
            //send(new Ping());
            break;
        }
      }

      Console.WriteLine("Messagereader ended");
    }

    private void LogOnNewLine(string message)
    {
      //if (Console.CursorLeft > 0)
        //Console.WriteLine();
      Console.WriteLine(message);
    }
    private void LogOnNewLine(string message, params object[] args)
    {
      //if (Console.CursorLeft > 0)
        //Console.WriteLine();
      Console.WriteLine(message, args);
    }

  }
}

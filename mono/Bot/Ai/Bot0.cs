﻿using System;
using System.Reactive.Linq;
using Bot.Connection;
using Bot.Messages;

namespace Bot.Ai
{
  public class Bot0 : BotBase
  {
    public Bot0(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {
    }

    protected override void Setup(IObservable<BaseMessage> messages)
    {
      messages.OfType<GameStartMessage>();

      var carPositionsSubscription = messages.OfType<CarPositionsMessage>().Subscribe(HandleCarPositions);
      _disposables.Add(carPositionsSubscription);
    }

    private void HandleCarPositions(CarPositionsMessage message)
    {
      SendMessage(new ThrottleMessage()
      {
        Data = 0.5d
      });
    }



  }
}

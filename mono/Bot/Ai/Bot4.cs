﻿using System;
using Bot.Connection;
using Bot.Messages;

namespace Bot.Ai
{
  public class Bot4 : BotAnalyzerBase
  {

    public Bot4(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {
    }

    protected override void Setup(IObservable<BaseMessage> messages)
    {
      base.Setup(messages);
    }

    private int fase = 0;
    private bool onstraight = false;
    private bool isbraking = false;
    private bool skip = true;
    protected override void HandleGameTick(CarPositionsMessage message)
    {
      if (message.GameTick == 10 || message.GameTick == 200)
      {
        SendMessage(new SwitchLaneMessage()
        {
          Data = SwitchLane.Right,
          GameTick = message.GameTick
        });
        return;
      }

      var throttle = 1d;

      var carAnalyzer = Analyzer[BotColor];

      var trackReader = carAnalyzer.GetTrackReader();

      var currentSpeed = carAnalyzer.LastTick.Speed;
      var currentAcceleration = carAnalyzer.LastTick.Acceleration;

      var targetSpeed = int.MaxValue;
      var nextCornerTargetSpeed = 1m;

      var distanceToCorner = GetDistanceToCorner(trackReader, carAnalyzer.LastTick.InPieceDistance);


      if (trackReader[0].Way != Way.Straight)
      { //in bocht 
        //throttle = 0.81m;
        if (carAnalyzer.LastTick.Angle > 57)
        {
            //throttle -= 0.01m;
        }
        else
        {
            if (currentSpeed > 9.22d)
                throttle -= 0.63d;   

                
        }

      }
      else if (currentSpeed > 2)
      { //recht
          //turbo
        if (distanceToCorner < 19)
          throttle = 0.95d;
      }



      //Console.WriteLine(@"A {0:000.000}", carAnalyzer.Angle);

     // Console.WriteLine(@"A {0:000.000} {1:000.000} {2:000.000} {3:0000} {4:0.0000}", currentSpeed, currentAcceleration, distanceToCorner, message.GameTick, throttle);

      
      
      SendMessage(new ThrottleMessage()
      {
        Data = throttle,
        GameTick = message.GameTick
      });
    }

    private double GetDistanceToCorner(TrackReader.RelativeTrackReader trackReader, double inPieceDistance)
    {
      var currentPiece = trackReader[0];
      if (currentPiece.Way != Way.Straight)
        return 0d;

      var result = currentPiece.Length - inPieceDistance;
      
      var i = 1;
      while (trackReader[i].Way != Way.Straight 
          && i != currentPiece.Index)
      {
        result += trackReader[i].Length;
        i++;
      }
      return result;
    }
  }
}

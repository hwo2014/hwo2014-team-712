﻿using System;
using Bot.Connection;
using Bot.Messages;

namespace Bot.Ai
{
  public class Bot3 : BotAnalyzerBase
  {

    public Bot3(IConnection connection, BaseMessage joinMessage)
      : base(connection, joinMessage)
    {
    }

    protected override void Setup(IObservable<BaseMessage> messages)
    {
      base.Setup(messages);
    }

    private int fase = 0;
    private bool onstraight = false;
    private bool isbraking = false;
    private bool skip = true;
    protected override void HandleGameTick(CarPositionsMessage message)
    {
      var throttle = 0.3d;

      var carAnalyzer = Analyzer[BotColor];
      var currentSpeed = carAnalyzer.LastTick.Speed;
      var currentAcceleration = carAnalyzer.LastTick.Acceleration;

      var index = carAnalyzer.LastTick.RacePieceIndex;
      if (carAnalyzer.LastTick.PieceIndex >= 35)
      {
        if (!onstraight)
        {
          throttle = 0;
          if (currentSpeed <= 0.5d)
            onstraight = true;
        }
        else
        {
          throttle = 1;
        }

        isbraking = false;
        skip = false;
        Console.WriteLine(@"A {0:000.000} {1:000.000} ", currentSpeed, currentAcceleration);
      }

      if (carAnalyzer.LastTick.PieceIndex <= 3 && !skip)
      {
        throttle = 0;

        if (currentSpeed <= 0.5d)
          skip = true;

        onstraight = false;
        Console.WriteLine(@"B {0:000.000} {1:000.000} ", currentSpeed, currentAcceleration);
      }




      SendMessage(new ThrottleMessage()
      {
        Data = throttle,
        GameTick = message.GameTick
      });
    }

  }
}

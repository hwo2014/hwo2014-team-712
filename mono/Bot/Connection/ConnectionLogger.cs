﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Bot.Connection
{
  public class ConnectionLogger : IConnection
  {
    private readonly IConnection _connection;
    private readonly string _filename;
    private StringBuilder _log;

    public ConnectionLogger(IConnection connection, string filename)
    {
      _connection = connection;
      _filename = filename;
      _log = new StringBuilder();
    }

    public IEnumerable<string> ReadLines()
    {
      foreach (var line in _connection.ReadLines())
      {
        _log.AppendLine(">> " + line);
        yield return line;
      }
    }

    public void Write(string message)
    {
      _log.AppendLine("<< " + message);
      _connection.Write(message);
      
    }

    public void Save()
    {
      File.WriteAllText(_filename, _log.ToString());
    }
  }
}

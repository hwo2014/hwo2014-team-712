﻿using System.Collections.Generic;

namespace Bot.Connection
{
  public interface IConnection
  {
    IEnumerable<string> ReadLines(); 
    void Write(string message);
  }
}
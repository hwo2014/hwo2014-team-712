﻿using System.Collections.Generic;
using System.IO;

namespace Bot.Connection
{
  public class NetworkConnection : IConnection
  {
    private readonly StreamReader _reader;
    private readonly StreamWriter _writer;

    public NetworkConnection(StreamReader reader, StreamWriter writer)
    {
      _reader = reader;
      _writer = writer;
    }

    public IEnumerable<string> ReadLines()
    {
      string line;
      while ((line = _reader.ReadLine()) != null)
        yield return line;
    }

    public void Write(string message)
    {
      _writer.WriteLine(message);
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bot;
using Bot.Ai;
using Bot.Messages;

namespace CustomBotStart
{
  class Program
  {
    static void Main(string[] args)
    {
      try
      {
        var bots = new List<Task>();

        // Default
        //var bot1 = StartBot<Bot1>(@"testserver.helloworldopen.com", 8091, @"Lakerfield", @"mPh+tevAyjGa9A");
        //bots.Add(bot1);

        // keimola
        //var race = new RaceData() { CarCount = 1, TrackName = "keimola" };
        //bots.Add(StartBot<Bot3>(@"testserver.helloworldopen.com", 8091, @"Lakerfield-Bot3", @"mPh+tevAyjGa9A", JoinType.CreateRace, race));

        // germany
        //var race = new RaceData() { CarCount = 1, TrackName = "germany" };
        //bots.Add(StartBot<Bot1>(@"testserver.helloworldopen.com", 8091, @"Lakerfield1", @"mPh+tevAyjGa9A", JoinType.CreateRace, race));

        //// usa
        //var race = new RaceData() { CarCount = 1, TrackName = "usa" };
        //bots.Add(Engine.StartBot<Bot4>(@"testserver.helloworldopen.com", 8091, @"Lakerfield-Bot4", @"mPh+tevAyjGa9A", JoinType.CreateRace, race));

        // france
        //var race = new RaceData() { CarCount = 1, TrackName = "france" };
        //bots.Add(Engine.StartBot<Bot0>(@"testserver.helloworldopen.com", 8091, @"SoerTest", @"mPh+tevAyjGa9A", JoinType.CreateRace, race));

        // x bots race
        var race = new RaceData()
        {
          CarCount = 3,
          TrackName = "keimola",
          //TrackName = "germany",
          Password = "Morguh2"
        };
        bots.Add(Engine.StartBot<Bot2>(@"testserver.helloworldopen.com", 8091, @"Lakerfield2", @"mPh+tevAyjGa9A", JoinType.JoinRace, race));
        bots.Add(Engine.StartBot<Bot0>(@"testserver.helloworldopen.com", 8091, @"Lakerfield0a", @"mPh+tevAyjGa9A", JoinType.JoinRace, race));
        bots.Add(Engine.StartBot<Bot0>(@"testserver.helloworldopen.com", 8091, @"Lakerfield0b", @"mPh+tevAyjGa9A", JoinType.JoinRace, race));
        Task.WaitAll(bots.ToArray());
      }
      catch (Exception exception)
      {
        Console.WriteLine(exception.Message);
      }

      Console.WriteLine("Press any key to quit...");
      Console.ReadKey();
    }


  }
}

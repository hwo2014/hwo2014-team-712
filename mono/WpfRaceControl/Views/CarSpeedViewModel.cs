﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Bot.Ai;
using WpfRaceControl.Annotations;
using WpfRaceControl.Controls;

namespace WpfRaceControl.Views
{
  public class CarSpeedViewModel : INotifyPropertyChanged
  {
    private ObservableCollection<MyPoint> _points;
    public ObservableCollection<MyPoint> Points
    {
      get { return _points; }
      set
      {
        if (Equals(value, _points)) return;
        _points = value;
        OnPropertyChanged();
      }
    }

    private int _xMaxValue;
    public int XMaxValue
    {
      get { return _xMaxValue; }
      set
      {
        if (Equals(value, _xMaxValue)) return;
        _xMaxValue = value;
        OnPropertyChanged();
      }
    }

    public CarSpeedViewModel()
    {
      Points = new ObservableCollection<MyPoint>();
      XMaxValue = 1000;
    }


    private MyPoint Convert(PositionAnalyzerCarTick tick)
    {
      var result = new MyPoint();
      result.X = tick.GameTick;
      result.Speed = tick.Speed;
      result.Acceleration = tick.Acceleration;
      result.Angle = tick.Angle / 15;
      result.LogInfo1 = tick.LogInfo1;
      result.PieceIndex = -tick.PieceIndex % 4;
      result.Throttle = tick.Throttle * 2 - 4;
      return result;
    }


    public class MyPoint
    {
      public int X { get; set; }
      public double Speed { get; set; }
      public double Acceleration { get; set; }
      public double Angle { get; set; }
      public double LogInfo1 { get; set; }
      public int PieceIndex { get; set; }
      public double Throttle { get; set; }
    }

    public void Update(PositionAnalyzerCarTick[] positionAnalyzerCarTicks)
    {
      

      Points = new ObservableCollection<MyPoint>(positionAnalyzerCarTicks.Select(Convert));

      //Points.AddRange(positionAnalyzerCarTicks.Skip(Points.Count).Select(Convert).ToList());

      //for (int i = Points.Count; i < positionAnalyzerCarTicks.Length; i++)
      //{
      //  Points.Add(Convert(positionAnalyzerCarTicks[i]));
      //}

    }



    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      var handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

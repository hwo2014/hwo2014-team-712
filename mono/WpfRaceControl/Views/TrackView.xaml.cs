﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bot;
using Bot.Messages;
using Bot.Models;

namespace WpfRaceControl.Views
{
  /// <summary>
  /// Interaction logic for TrackView.xaml
  /// </summary>
  public partial class TrackView : UserControl
  {
    //private StringBuilder _log = new StringBuilder();

    public TrackView()
    {
      InitializeComponent();

      //Loaded += TrackView_Loaded;
    }

    public static readonly DependencyProperty RaceProperty = DependencyProperty.Register(
      "Race", typeof(Race), typeof(TrackView), new PropertyMetadata(default(Race), RacePropertyChangedCallback));

    public Race Race
    {
      get { return (Race)GetValue(RaceProperty); }
      set { SetValue(RaceProperty, value); }
    }

    private static void RacePropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
    {
      var trackView = (TrackView)dependencyObject;
      var race = dependencyPropertyChangedEventArgs.NewValue as Race;
      if (race == null)
        return;
      trackView.BuildTrack(race);
    }


    public static readonly DependencyProperty CarPositionsProperty = DependencyProperty.Register(
      "CarPositions", typeof(CarPositionsMessage), typeof(TrackView), new PropertyMetadata(default(CarPositionsMessage), CarPositionsPropertyChangedCallback));

    public CarPositionsMessage CarPositions
    {
      get { return (CarPositionsMessage)GetValue(CarPositionsProperty); }
      set { SetValue(CarPositionsProperty, value); }
    }

    private static void CarPositionsPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
    {
      var trackView = (TrackView)dependencyObject;
      var carPositions = dependencyPropertyChangedEventArgs.NewValue as CarPositionsMessage;
      if (carPositions == null)
        return;

      trackView.UpdateCarDimensions();

      var race = trackView.Race;
      foreach (var carPosition in carPositions.Data)
      {
        var car = trackView._carInfos.FirstOrDefault(c => c.IdColor == carPosition.Id.Color);
        if (car != null)
          trackView.UpdateLocation(car, carPosition, race);
      }
    }

    private CarInfo[] _carInfos;

    public class CarInfo
    {
      public string IdColor { get; set; }
      public string Name { get; set; }
      public Rectangle Element { get; set; }

      public RotateTransform RotateTransform { get; set; }

      public double Width { get; set; }
      public double Length { get; set; }
      public double Offset { get; set; }

    }

    private bool _updateCarDimensions = true;
    private void UpdateCarDimensions()
    {
      if (!_updateCarDimensions)
        return;
      _updateCarDimensions = false;

      _carInfos = Race.Cars.Select(input =>
      {
        var result = new CarInfo();
        result.IdColor = input.Color;
        result.Name = input.Name;

        var car = result.Element = new Rectangle();
        result.Element.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString(input.Color));

        result.Width = Convert.ToDouble(input.Width);
        result.Length = Convert.ToDouble(input.Length);
        result.Offset = Convert.ToDouble(input.Offset);

        car.Width = result.Length;
        car.Height = result.Width;

        car.RenderTransformOrigin = new Point(0, 0);//(_carLength-carOffset)/_carLength, 0.5);

        TranslateTransform myTranslate = new TranslateTransform();
        myTranslate.X = -(car.Width - result.Offset);
        myTranslate.Y = -car.Height / 2;

        result.RotateTransform = new RotateTransform();
        result.RotateTransform.Angle = 0;
        result.RotateTransform.CenterX = 0;//car.Width - _carOffset;
        result.RotateTransform.CenterY = 0;//car.Height / 2;

        // Create a TransformGroup to contain the transforms 
        // and add the transforms to it. 
        TransformGroup myTransformGroup = new TransformGroup();
        myTransformGroup.Children.Add(myTranslate);
        myTransformGroup.Children.Add(result.RotateTransform);

        // Associate the transforms to the object 
        car.RenderTransform = myTransformGroup;
        
        TrackCanvas.Children.Add(car);

        return result;
      }).ToArray();



    }


    private void UpdateLocation(CarInfo car, CarPositionsMessage.CarPosition carPosition, Race race)
    {
      var position = carPosition.PiecePosition;
      var piece = race.Track.Pieces[position.PieceIndex];
      var lane = piece.Lanes.First(l => l.Index == position.Lane.StartLaneIndex);

      var laneStartX = CalcX(piece.GraphX, Convert.ToDouble(lane.DistanceFromCenter), piece.GraphA + 90);
      var laneStartY = CalcY(piece.GraphY, Convert.ToDouble(lane.DistanceFromCenter), piece.GraphA + 90);

      double x;
      double y;
      double angle;
      if (piece.Way == Way.Straight)
      {
        x = CalcX(laneStartX, Convert.ToDouble(position.InPieceDistance), piece.GraphA);
        y = CalcY(laneStartY, Convert.ToDouble(position.InPieceDistance), piece.GraphA);
        angle = 0;
      }
      else
      {
        var radius = piece.Way == Way.Left
          ? Convert.ToDouble(piece.Radius + lane.DistanceFromCenter)
          : Convert.ToDouble(piece.Radius - lane.DistanceFromCenter);

        var centerX = CalcX(laneStartX, radius, piece.GraphA + 90 * Math.Sign(piece.Angle));
        var centerY = CalcY(laneStartY, radius, piece.GraphA + 90 * Math.Sign(piece.Angle));

        //CenterPoint.SetValue(Canvas.LeftProperty, centerX);
        //CenterPoint.SetValue(Canvas.TopProperty, centerY);

        var omtrek = GetOmtrek(radius);
        angle = Convert.ToDouble(position.InPieceDistance) / omtrek * 360 * Math.Sign(piece.Angle);

        x = CalcX(centerX, radius, piece.GraphA + angle - 90 * Math.Sign(piece.Angle));
        y = CalcY(centerY, radius, piece.GraphA + angle - 90 * Math.Sign(piece.Angle));

        //_log.AppendLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}", radius, piece.GraphA, omtrek, position.InPieceDistance, angle));
      }

      car.Element.SetValue(Canvas.LeftProperty, x);
      car.Element.SetValue(Canvas.TopProperty, y);

      //car.Element.Stroke = car.Turbo ? Brushes.DeepPink : Brushes.Transparent;

      if (car.RotateTransform != null)
        car.RotateTransform.Angle = piece.GraphA + angle + Convert.ToDouble(carPosition.Angle);
    }

    public double GetOmtrek(double radius)
    {
      return 2 * radius * Math.PI;
    }


    void TrackView_Loaded(object sender, RoutedEventArgs e)
    {
      #region Test code
      //var track = new Bot.Models.Track();
      //track.Pieces = new TrackPiece[]
      //{
      //  new TrackPiece()
      //  {
      //    Index=0,
      //    Length=100,
      //    Way=Way.Straight,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = -20,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 0,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 10,
      //      },
      //    }
      //  },
      //  new TrackPiece()
      //  {
      //    Index=1,
      //    Angle=90,
      //    Radius=50,
      //    Way=Way.Right,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = -20,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 0,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 10,
      //      },
      //    }
      //  },
      //  new TrackPiece()
      //  {
      //    Index=0,
      //    Length=50,
      //    Way=Way.Straight,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = -20,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 0,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 10,
      //      },
      //    }
      //  },

      //  new TrackPiece()
      //  {
      //    Index=1,
      //    Angle=90,
      //    Radius=50,
      //    Way=Way.Right,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = -20,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 0,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 10,
      //      },
      //    }
      //  },
      //  new TrackPiece()
      //  {
      //    Index=0,
      //    Length=50,
      //    Way=Way.Straight,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = -20,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 0,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 10,
      //      },
      //    }
      //  },

      //  new TrackPiece()
      //  {
      //    Index=1,
      //    Angle=90,
      //    Radius=50,
      //    Way=Way.Right,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = -20,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 0,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 10,
      //      },
      //    }
      //  },
      //  new TrackPiece()
      //  {
      //    Index=0,
      //    Length=50,
      //    Way=Way.Straight,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = -20,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 0,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 10,
      //      },
      //    }
      //  },

      //  new TrackPiece()
      //  {
      //    Index=1,
      //    Angle=-30,
      //    Radius=50,
      //    Way=Way.Left,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = -20,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 0,
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        DistanceFromCenter = 10,
      //      },
      //    }
      //  },
      //  new TrackPiece()
      //  {
      //    Index=2,
      //    Length=100,
      //    Way=Way.Straight,
      //    Lanes = new TrackPieceLane[]
      //    {
      //      new TrackPieceLane()
      //      {
      //        Length=100,
      //        DistanceFromCenter = -20,
      //        Way = Way.Straight
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        Length=100,
      //        DistanceFromCenter = 0,
      //        Way = Way.Straight
      //      }, 
      //      new TrackPieceLane()
      //      {
      //        Length=100,
      //        DistanceFromCenter = 10,
      //        Way = Way.Straight
      //      },
      //    }
      //  }     
      //};

      //BuildTrack(track);
      #endregion
    }

    public void BuildTrack(Race race)
    {
      TrackCanvas.Children.Clear();
      _updateCarDimensions = true;

      var minX = 0d;
      var maxX = 0d;
      var minY = 0d;
      var maxY = 0d;

      var track = race.Track;
      double x = Convert.ToDouble(track.StartX);
      double y = Convert.ToDouble(track.StartY);
      double angle = Convert.ToDouble(track.StartAngle) - 90;

      foreach (var piece in track.Pieces)
      {
        piece.GraphX = x;
        piece.GraphY = y;
        piece.GraphA = angle;

        AddPiece(ref x, ref y, ref angle, piece);

        if (minX > x)
          minX = x;
        if (maxX < x)
          maxX = x;
        if (minY > y)
          minY = y;
        if (maxY < y)
          maxY = y;
      }

      // Add border around track
      var trackWidth = 0d;
      var car = race.Cars.FirstOrDefault();
      if (car != null)
        trackWidth = Convert.ToDouble(car.Width);

      WrapCanvas.Width = maxX - minX + 2 * trackWidth;
      WrapCanvas.Height = maxY - minY + 2 * trackWidth;

      TrackCanvas.SetValue(Canvas.LeftProperty, -minX + trackWidth);
      TrackCanvas.SetValue(Canvas.TopProperty, -minY + trackWidth);

      // Draw finish
      var piece0 = track.Pieces.First();
      var maxFromCenter = Convert.ToDouble(piece0.Lanes.Max(l => l.DistanceFromCenter));
      var minFromCenter = Convert.ToDouble(piece0.Lanes.Min(l => l.DistanceFromCenter));

      var x1 = CalcX(x, maxFromCenter + trackWidth, angle + 90);
      var y1 = CalcY(y, maxFromCenter + trackWidth, angle + 90);

      var x2 = CalcX(x, minFromCenter - trackWidth, angle + 90);
      var y2 = CalcY(y, minFromCenter - trackWidth, angle + 90);

      Add(x1, y1, x2, y2, Brushes.Green, 8);
    }

    private void AddPiece(ref double x, ref double y, ref double angle, TrackPiece piece)
    {
      var startX = x;
      var startY = y;
      var startA = angle;

      foreach (var lane in piece.Lanes)
      {
        switch (piece.Way)
        {
          case Way.Left:
            AddCurve(startX, startY, Convert.ToDouble(-lane.DistanceFromCenter), startA, Convert.ToDouble(piece.Angle), Convert.ToDouble(piece.Radius + lane.DistanceFromCenter));
            break;
          case Way.Straight:
            AddLine(startX, startY, Convert.ToDouble(lane.DistanceFromCenter), startA, Convert.ToDouble(piece.Length));
            break;
          case Way.Right:
            AddCurve(startX, startY, Convert.ToDouble(lane.DistanceFromCenter), startA, Convert.ToDouble(piece.Angle), Convert.ToDouble(piece.Radius - lane.DistanceFromCenter));
            break;
          default:
            throw new ArgumentOutOfRangeException();
        }
      }

      switch (piece.Way)
      {
        case Way.Straight:
          x = CalcX(startX, Convert.ToDouble(piece.Length), startA);
          y = CalcY(startY, Convert.ToDouble(piece.Length), startA);
          break;
        case Way.Left:
        case Way.Right:
          var centerX = CalcX(x, Convert.ToDouble(piece.Radius), startA + 90 * Math.Sign(piece.Angle));
          var centerY = CalcY(y, Convert.ToDouble(piece.Radius), startA + 90 * Math.Sign(piece.Angle));
          x = CalcX(centerX, Convert.ToDouble(piece.Radius), startA + Convert.ToDouble(piece.Angle) - 90 * Math.Sign(piece.Angle));
          y = CalcY(centerY, Convert.ToDouble(piece.Radius), startA + Convert.ToDouble(piece.Angle) - 90 * Math.Sign(piece.Angle));
          //Add(centerX, centerY, x, y, Brushes.Green);
          break;
        default:
          throw new ArgumentOutOfRangeException();
      }
      angle = angle + Convert.ToDouble(piece.Angle);
    }

    private void AddCurve(double startX, double startY, double distanceFromCenter, double startA, double angle, double radius)
    {
      var x1 = CalcX(startX, distanceFromCenter, startA + 90 * Math.Sign(angle));
      var y1 = CalcY(startY, distanceFromCenter, startA + 90 * Math.Sign(angle));

      var centerX = CalcX(x1, radius, startA + 90 * Math.Sign(angle));
      var centerY = CalcY(y1, radius, startA + 90 * Math.Sign(angle));

      var x2 = CalcX(centerX, radius, startA + angle - 90 * Math.Sign(angle));
      var y2 = CalcY(centerY, radius, startA + angle - 90 * Math.Sign(angle));

      //Add(centerX, centerY, x1, y1);
      //Add(centerX, centerY, x2, y2);

      var figure = new PathFigure();
      figure.StartPoint = new Point(x1, y1);

      var arc = new ArcSegment();

      arc.IsLargeArc = false;
      arc.Point = new Point(x2, y2);
      arc.Size = new Size(radius, radius);
      arc.SweepDirection = angle > 0 ? SweepDirection.Clockwise : SweepDirection.Counterclockwise;

      figure.Segments.Add(arc);

      Add(new PathGeometry(new[] { figure }));
    }

    private void AddLine(double startX, double startY, double distanceFromCenter, double startA, double length)
    {
      var x1 = CalcX(startX, distanceFromCenter, startA + 90);
      var y1 = CalcY(startY, distanceFromCenter, startA + 90);

      var x2 = CalcX(x1, length, startA);
      var y2 = CalcY(y1, length, startA);

      Add(x1, y1, x2, y2);
    }

    private double CalcX(double startX, double length, double angle)
    {
      return startX + length * Math.Cos(angle / 180 * Math.PI);
    }

    private double CalcY(double startY, double length, double angle)
    {
      return startY + length * Math.Sin(angle / 180 * Math.PI);
    }

    private void Add(double x1, double y1, double x2, double y2, Brush brush = null, double strokeThickness = 2)
    {
      var line = new LineGeometry();

      line.StartPoint = new Point(x1, y1);
      line.EndPoint = new Point(x2, y2);

      Add(line, brush, strokeThickness);
    }

    private void Add(Geometry geometry, Brush brush = null, double strokeThickness = 2)
    {
      var path = new Path();

      path.Data = geometry;
      path.Stroke = brush ?? Brushes.Red;
      path.StrokeThickness = strokeThickness;
      path.Fill = Brushes.Transparent;

      TrackCanvas.Children.Add(path);
    }


  }
}

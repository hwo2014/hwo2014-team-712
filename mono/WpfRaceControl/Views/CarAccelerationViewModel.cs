﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Bot.Ai;
using WpfRaceControl.Annotations;
using WpfRaceControl.Controls;
using WpfRaceControl.Helpers;

namespace WpfRaceControl.Views
{
  public class CarAccelerationViewModel : INotifyPropertyChanged
  {
    private ObservableCollection<MyPoint> _points;
    public ObservableCollection<MyPoint> Points
    {
      get { return _points; }
      set
      {
        if (Equals(value, _points)) return;
        _points = value;
        OnPropertyChanged();
      }
    }


    private int _xMaxValue;
    public int XMaxValue
    {
      get { return _xMaxValue; }
      set
      {
        if (Equals(value, _xMaxValue)) return;
        _xMaxValue = value;
        OnPropertyChanged();
      }
    }

    public CarAccelerationViewModel()
    {
      Points = new ObservableCollection<MyPoint>();
      XMaxValue = 1000;
    }


    private MyPoint Convert(Tuple<PositionAnalyzerCarTick, PositionAnalyzerCarTick> ticks)
    {
      var result = new MyPoint();
      result.X = ticks.Item2.GameTick;
      result.Speed = System.Convert.ToDouble(ticks.Item2.Speed - ticks.Item1.Speed);
      result.Acceleration = System.Convert.ToDouble(ticks.Item2.Acceleration - ticks.Item1.Acceleration);
      result.Angle = System.Convert.ToDouble(ticks.Item2.Angle - ticks.Item1.Angle);

      result.LogInfo1 = ticks.Item1.LogInfo1 / 5;
      result.PieceIndex = -(ticks.Item1.PieceIndex % 4)/4 - 2;
      result.Throttle = ticks.Item1.Throttle - 2;
      
      return result;
    }


    public class MyPoint
    {
      public int X { get; set; }
      public double Speed { get; set; }
      public double Acceleration { get; set; }
      public double Angle { get; set; }
      public double LogInfo1 { get; set; }
      public int PieceIndex { get; set; }
      public double Throttle { get; set; }
    }

    public void Update(PositionAnalyzerCarTick[] positionAnalyzerCarTicks)
    {
      try
      {
        Points = new ObservableCollection<MyPoint>(positionAnalyzerCarTicks.Buffer2().Select(Convert));
      }
      catch (Exception exception)
      {
        Console.WriteLine(exception.Message);
      }
    }



    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      var handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}

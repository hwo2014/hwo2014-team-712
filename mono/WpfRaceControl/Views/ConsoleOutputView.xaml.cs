﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfRaceControl.Shell;

namespace WpfRaceControl.Views
{
  /// <summary>
  /// Interaction logic for ConsoleOutputView.xaml
  /// </summary>
  public partial class ConsoleOutputView : Window
  {
    public ConsoleOutputView()
    {
      InitializeComponent();

      Loaded += OnLoaded;
    }

    private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
    {
      // Instantiate the writer
      var _writer = new TextBoxStreamWriter(ConsoleOutput);
      // Redirect the out Console stream
      Console.SetOut(_writer);
    }
  }
}

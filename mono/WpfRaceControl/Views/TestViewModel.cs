﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Bot;
using Bot.Ai;
using Bot.Ai.BotV2;
using Bot.Connection;
using Bot.Messages;
using Bot.Models;
using Caliburn.Micro;

namespace WpfRaceControl.Views
{
  public class TestViewModel : Screen, IMainView
  {
    private IBotBase _currentBot;
    private IBotAnalyzerBase _currentAnalyzerBot;

    public string[] Servers
    {
      get
      {
        return new[]
        {
          @"testserver.helloworldopen.com (Load balancer)",
          @"hakkinen.helloworldopen.com (R2)",
          @"senna.helloworldopen.com (R1)",
          @"webber.helloworldopen.com (R3)",
          @"prost.helloworldopen.com (constant physics)"
        };
      }
    }
    public string SelectedServer { get; set; }
    public string ServerHostname
    {
      get { return SelectedServer.Split(' ').First(); }
    }


    public string[] Tracks { get { return new[] { @"keimola", @"germany", @"usa", @"france", @"elaeintarha", @"imola", @"england", @"suzuka" }; } }
    public string SelectedTrack { get; set; }

    public Type[] BotTypes { get; set; }
    public string[] Bots { get; set; }
    public string SelectedBot { get; set; }

    public bool AddTraffic { get; set; }

    public bool Join { get; set; }
    public int JoinCars { get; set; }
    public string JoinPassword { get; set; }

    private Race _race;
    public Race Race
    {
      get { return _race; }
      set
      {
        if (Equals(value, _race)) return;
        _race = value;
        NotifyOfPropertyChange(() => Race);
      }
    }

    private CarPositionsMessage _lastCarPositions;
    public CarPositionsMessage LastCarPositions
    {
      get { return _lastCarPositions; }
      set
      {
        if (Equals(value, _lastCarPositions)) return;
        _lastCarPositions = value;
        NotifyOfPropertyChange(() => LastCarPositions);
      }
    }

    private GameTickResponse _lastGameTickResponse;
    public GameTickResponse LastGameTickResponse
    {
      get { return _lastGameTickResponse; }
      set
      {
        if (Equals(value, _lastGameTickResponse)) return;
        _lastGameTickResponse = value;
        NotifyOfPropertyChange(() => LastGameTickResponse);
        NotifyOfPropertyChange(() => Piece);
        NotifyOfPropertyChange(() => Lane);
      }
    }

    public TrackPiece Piece
    {
      get
      {
        if (Race == null)
          return null;
        if (LastGameTickResponse == null)
          return null;
        return Race.Track.Pieces[LastGameTickResponse.CarInfo.PieceIndex];
      }
    }

    public TrackPieceLane Lane
    {
      get
      {
        if (Race == null)
          return null;
        if (Piece == null)
          return null;
        return Piece.Lanes[LastGameTickResponse.CarInfo.LaneStart];
      }
    }


    public TestViewModel()
    {
      SelectedTrack = Tracks.Skip(2).First();
      SelectedServer = Servers.Last();

      var botBaseType = typeof(IBotBase);
      BotTypes = botBaseType.Assembly.GetTypes().Where(botBaseType.IsAssignableFrom).Where(b => !b.IsAbstract).ToArray();
      Bots = BotTypes.Select(t => t.Name).ToArray();
      SelectedBot = Bots.OrderBy(b => b).Where(b => b.StartsWith("Bot")).LastOrDefault();
    }


    public void StartRace2()
    {
      ShowGraph(settings: new Dictionary<string, object>
      {
        //{ "WindowStyle", WindowStyle.None},
        //{ "ShowInTaskbar", false},
        //{ "AllowsTransparency", true},
        //{ "Background", new SolidColorBrush(Colors.Transparent)},
        {"Top", SystemParameters.WorkArea.Top},
        {"Left", SystemParameters.WorkArea.Left + SystemParameters.WorkArea.Width/2},
        {"Width", SystemParameters.WorkArea.Width/2},
        {"Height", SystemParameters.WorkArea.Height},
      });

      //ShowGraph2(settings: new Dictionary<string, object>
      //{
      //  {"Top", SystemParameters.WorkArea.Top+SystemParameters.WorkArea.Height/2},
      //  {"Left", SystemParameters.WorkArea.Left + SystemParameters.WorkArea.Width/2},
      //  {"Width", SystemParameters.WorkArea.Width/2},
      //  {"Height", SystemParameters.WorkArea.Height/2},
      //  //{"Top", 0},
      //  //{"Left", SystemParameters.PrimaryScreenWidth},
      //  //{"Width", 1280},
      //  //{"Height", 1024},
      //});

      Application.Current.MainWindow.Top = SystemParameters.WorkArea.Top;
      Application.Current.MainWindow.Left = SystemParameters.WorkArea.Left;
      Application.Current.MainWindow.Width = SystemParameters.WorkArea.Width/2;
      Application.Current.MainWindow.Height = SystemParameters.WorkArea.Height/4*3;

      ShowConsoleOutput(settings: new Dictionary<string, object>
      {
        {"Top", SystemParameters.WorkArea.Top + SystemParameters.WorkArea.Height/4*3},
        {"Left", SystemParameters.WorkArea.Left},
        {"Width", SystemParameters.WorkArea.Width/2},
        {"Height", SystemParameters.WorkArea.Height/4},
      });

      //var w = new Window();
      //w.Left = System.Windows.SystemParameters.PrimaryScreenWidth/2;
      //w.Top = 0;
      //w.Width = System.Windows.SystemParameters.PrimaryScreenWidth/2;
      //w.Height = System.Windows.SystemParameters.PrimaryScreenHeight;

      StartRace();
    }

    public void Terminate()
    {
      Application.Current.Shutdown();
    }

    public async void StartRace()
    {
      if (!AddTraffic)
      {
        var joinType = JoinType.CreateRace;
        var race = new RaceData() { CarCount = 1, TrackName = SelectedTrack };
        if (Join)
        {
          joinType = JoinType.JoinRace;
          race.CarCount = JoinCars;
          race.Password = JoinPassword;
        }

        await Engine.StartBot(ServerHostname, 8091, @"Lakerfield", @"mPh+tevAyjGa9A", joinType, race, createBot: CreateBot);
      }
      else
      {
        var race = new RaceData() { CarCount = 3, TrackName = SelectedTrack, Password = "Bertus2" };
        var task2 = Engine.StartBot<BotV2Number0>(@"hakkinen.helloworldopen.com", 8091, @"Bot0", @"mPh+tevAyjGa9A", JoinType.JoinRace, race);
        //var task3 = Engine.StartBot<BotV2Number1>(@"hakkinen.helloworldopen.com", 8091, @"Bot1", @"mPh+tevAyjGa9A", JoinType.JoinRace, race);
        //var task4 = Engine.StartBot<BotV2Number1>(@"hakkinen.helloworldopen.com", 8091, @"Bot3", @"mPh+tevAyjGa9A", JoinType.JoinRace, race);
        var task5 = Engine.StartBot<BotV2Number2>(@"hakkinen.helloworldopen.com", 8091, @"Bot4", @"mPh+tevAyjGa9A", JoinType.JoinRace, race);
        var task1 = Engine.StartBot(@"hakkinen.helloworldopen.com", 8091, @"Lakerfield", @"mPh+tevAyjGa9A", JoinType.JoinRace, race, createBot: CreateBot);
        await Task.WhenAll(task1, task2,  task5);//task3, task4,
      }

      MessageBox.Show("Bot(s) disconnected");
    }



    private IBotBase CreateBot(IConnection connection, BaseMessage joinMessage)
    {
      var botType = BotTypes.First(t => t.Name == SelectedBot);

      var bot = _currentBot = Activator.CreateInstance(botType, connection, joinMessage) as IBotBase;
      //new Bot3(connection, joinMessage);
      _currentAnalyzerBot = _currentBot as IBotAnalyzerBase;

      bot.ObservableCreated += bot_ObservableCreated;


      return bot;
    }

    void bot_ObservableCreated(IBotBase sender, IObservable<BaseMessage> observable, IObservable<GameTickResponse> gameTickResponses)
    {
      var gameInitSubscription = observable.OfType<GameInitMessage>().ObserveOnDispatcher().Subscribe(HandleMessage);
      //_disposables.Add(gameInitSubscription);

      var subscription1 = observable.OfType<CarPositionsMessage>().ObserveOnDispatcher().Subscribe(HandleMessage);
      var subscription2 = observable.OfType<CarPositionsMessage>().Buffer(TimeSpan.FromMilliseconds(1000)).ObserveOnDispatcher().Subscribe(HandleMessage);


      if (gameTickResponses != null)
      {
        var subscription3 = gameTickResponses.ObserveOnDispatcher().Subscribe(HandleMessage);
      }
    }

    private void HandleMessage(GameInitMessage message)
    {
      Race = _currentBot.Race;

    }

    private void HandleMessage(CarPositionsMessage message)
    {
      LastCarPositions = message;
    }

    private void HandleMessage(GameTickResponse gameTickResponse)
    {
      LastGameTickResponse = gameTickResponse;
    }

    private void HandleMessage(IList<CarPositionsMessage> message)
    {
      if (_currentAnalyzerBot == null)
        return;

      if (_carSpeedViewModel != null && _currentAnalyzerBot.Analyzer != null)
        _carSpeedViewModel.Update(_currentAnalyzerBot.Analyzer[_currentAnalyzerBot.BotColor].Ticks.ToArray());
      if (_carAccelerationViewModel != null && _currentAnalyzerBot.Analyzer != null)
        _carAccelerationViewModel.Update(_currentAnalyzerBot.Analyzer[_currentAnalyzerBot.BotColor].Ticks.ToArray());
    }

    private CarSpeedViewModel _carSpeedViewModel;
    public void ShowGraph(IDictionary<string, object> settings = null)
    {
      if (_carSpeedViewModel == null)
        _carSpeedViewModel = new CarSpeedViewModel();

      IoC.Get<IWindowManager>().ShowWindow(_carSpeedViewModel, null, settings);
    }

    private CarAccelerationViewModel _carAccelerationViewModel;
    public void ShowGraph2(IDictionary<string, object> settings = null)
    {
      if (_carAccelerationViewModel == null)
        _carAccelerationViewModel = new CarAccelerationViewModel();

      IoC.Get<IWindowManager>().ShowWindow(_carAccelerationViewModel, null, settings);
    }

    private ConsoleOutputViewModel _consoleOutputViewModel;
    public void ShowConsoleOutput(IDictionary<string, object> settings = null)
    {
      if (_consoleOutputViewModel == null)
        _consoleOutputViewModel = new ConsoleOutputViewModel();

      IoC.Get<IWindowManager>().ShowWindow(_consoleOutputViewModel, null, settings);
    }

  }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using Caliburn.Micro;
using WpfRaceControl.Shell;
using WpfRaceControl.Views;

namespace WpfRaceControl
{
  public class AppBootstrapper : BootstrapperBase
  {
    SimpleContainer _container;

    public AppBootstrapper()
    {
      Start();
    }

    protected override void Configure()
    {
      Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

      _container = new SimpleContainer();

      _container.Singleton<IWindowManager, WindowManager>();
      _container.Singleton<IEventAggregator, EventAggregator>();
      _container.RegisterInstance(typeof(IShell), null, new ShellViewModel());


      _container.Singleton<TestViewModel>();
    }

    protected override object GetInstance(Type service, string key)
    {
      var instance = _container.GetInstance(service, key);
      if (instance != null)
        return instance;

      throw new InvalidOperationException("Could not locate any instances.");
    }

    protected override IEnumerable<object> GetAllInstances(Type service)
    {
      return _container.GetAllInstances(service);
    }

    protected override void BuildUp(object instance)
    {
      _container.BuildUp(instance);
    }

    protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
    {
      DisplayRootViewFor<IShell>();
    }
  }
}
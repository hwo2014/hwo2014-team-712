﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfRaceControl.Helpers
{
  public static class IEnumerableExtensions
  {


    public static IEnumerable<Tuple<T, T>> Buffer2<T>(this IEnumerable<T> input)
    {
      T cache = default(T);
      foreach (var item in input)
      {
        if (cache != null)
          yield return new Tuple<T, T>(cache, item);
        cache = item;
      }
    }

  }
}

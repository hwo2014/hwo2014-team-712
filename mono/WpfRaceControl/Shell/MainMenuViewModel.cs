﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using WpfRaceControl.Views;

namespace WpfRaceControl.Shell
{
  public class MainMenuViewModel : Conductor<IMainView>
  {


    public MainMenuViewModel()
    {
      MenuHomeClick();
    }


    public void MenuHomeClick()
    {
      ActivateItem<TestViewModel>();
    }




    public void ActivateItem<T>()
    {
      if (ActiveItem is T)
        return;

      var view = IoC.GetInstance(typeof(T), null) as IMainView;
      ActivateItem(view);
    }

    private bool _inActivation = false;
    public override void ActivateItem(IMainView item)
    {
      _inActivation = true;
      try
      {
        var activeItem = base.ActiveItem;
        if (activeItem is IMainViewAutoClose)
          DeactivateItem(activeItem, true);

        base.ActivateItem(item);
      }
      finally
      {
        _inActivation = false;
      }
    }

    public override void DeactivateItem(IMainView item, bool close)
    {
      base.DeactivateItem(item, close);

      if (_inActivation)
        return;
      MenuHomeClick();
    }

    public T GetInstance<T>()
    {
      return (T)IoC.GetInstance(typeof(T), null);
    }

  }
}

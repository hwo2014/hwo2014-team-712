﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace WpfRaceControl.Shell
{
  /// <summary>
  /// Interaction logic for ShellView.xaml
  /// </summary>
  public partial class ShellView : Window
  {
    public ShellView()
    {
      InitializeComponent();

      //Icon = new BitmapImage(new Uri("pack://application:,,,/Images/window_earth48-32-16.ico"));

      Loaded += OnLoaded;

    }

    private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
    {
      //ConsoleOutput.AppendText();

    }
  }
}

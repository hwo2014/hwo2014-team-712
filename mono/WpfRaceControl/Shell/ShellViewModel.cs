﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;

namespace WpfRaceControl.Shell
{
  public class ShellViewModel : Conductor<object>, IShell
  {
    public override string DisplayName
    {
      get { return "Lakerfield Race Control"; }
      set { }
    }

    public ShellViewModel()
    {
      TaskScheduler.UnobservedTaskException += TaskSchedulerUnobservedTaskException;
      AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
    }

    private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
    {
      var exception = unhandledExceptionEventArgs.ExceptionObject as Exception;
      if (exception != null)
      {
        MessageBox.Show(
          string.Format(@"Exeption:{2}{0}{2}{2}Stacktrace:{2}{1}",
            exception.Message,
            exception.StackTrace,
            Environment.NewLine));
        //ActivateItem(new Fouten.ExceptionViewModel(exception));
      }
    }

    void TaskSchedulerUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
    {
      var exception = e.Exception;
      MessageBox.Show(
        string.Format(@"Exeption:{2}{0}{2}{2}Stacktrace:{2}{1}",
          exception.Message,
          exception.StackTrace,
          Environment.NewLine));
      //ActivateItem(new Fouten.ExceptionViewModel(e.Exception));
      e.SetObserved();
    }

    protected override void OnViewReady(object view)
    {
      base.OnViewReady(view);

      var mainMenuViewModel = new MainMenuViewModel();
      ActivateItem(mainMenuViewModel);
    }


  }
}

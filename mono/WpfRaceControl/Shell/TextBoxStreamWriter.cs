﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfRaceControl.Shell
{
  public class TextBoxStreamWriter : TextWriter
  {
    readonly TextBox _output;

    public TextBoxStreamWriter(TextBox output)
    {
      _output = output;
    }

    public override void Write(char value)
    {
      base.Write(value);

      // When character data is written, append it to the text box.
      var app = Application.Current;
      if (app != null && app.Dispatcher != null)
        app.Dispatcher.Invoke(() => _output.AppendText(value.ToString()));
    }

    public override Encoding Encoding
    {
      get { return System.Text.Encoding.UTF8; }
    }
  }
}

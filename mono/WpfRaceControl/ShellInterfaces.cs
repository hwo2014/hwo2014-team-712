﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfRaceControl
{
  public interface IShell
  {
  }

  public interface IMainView
  {
  }

  public interface IMainViewAutoClose : IMainView
  {
  }

}

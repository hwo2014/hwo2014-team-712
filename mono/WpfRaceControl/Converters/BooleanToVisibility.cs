﻿using System;
using System.Windows;
using System.Windows.Data;

namespace WpfRaceControl.Converters
{
  public class BooleanToVisibility : IValueConverter
  {
    public object Convert(object value, Type targetType, object aParameter, System.Globalization.CultureInfo culture)
    {
      bool visible = (bool)value;

      string parameter = aParameter as string;
      if (!string.IsNullOrWhiteSpace(parameter))
      {
        if (parameter.ToUpper() == "NOT")
          visible = !visible;
      }

      if (aParameter as string != null && (aParameter as string).ToLower().Contains("inverse"))
        visible = !visible;

      if (visible)
        return Visibility.Visible;
      else
        return Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

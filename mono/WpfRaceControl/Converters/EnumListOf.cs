﻿using System;
using System.Windows.Markup;

namespace WpfRaceControl.Converters
{
  public class EnumListOf : MarkupExtension
  {
    private readonly Type _of;

    public EnumListOf(Type of)
    {
      _of = of;
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
      return typeof(EnumListConverter<>).MakeGenericType(_of);
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfRaceControl.Controls
{
  public class RangeObservableCollection<T> : ObservableCollection<T>
  {
    private bool _suppressNotification = false;

    protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
      if (!_suppressNotification)
        base.OnCollectionChanged(e);
    }

    public void AddRange(IList<T> list)
    {
      if (list == null)
        throw new ArgumentNullException("list");

      _suppressNotification = true;

      var insertFrom = this.Count;
      foreach (T item in list)
      {
        Add(item);
      }

      _suppressNotification = false;
      OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, list, insertFrom));
    }
  }
}

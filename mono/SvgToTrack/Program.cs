﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SvgToTrack
{
  class Program
  {
    static void Main(string[] args)
    {
      var text = File.ReadAllText(@"track.svg");
      string path = text.Replace(Environment.NewLine, "");
      
      string separators = @"(?=[A-Za-z])";
      var tokens = Regex.Split(path, separators).Where(t => !string.IsNullOrEmpty(t));

      // our "interpreter". Runs the list of commands and does something for each of them.
      foreach (string token in tokens.Take(5))
      {
        // note that Parse could throw an exception
        // if the path is not correct 
        SvgCommand c = SvgCommand.Parse(token);
        Console.WriteLine("doing something with command {0}", c.command);

        foreach (var f in c.arguments)
        {
          Console.WriteLine(f);
        }


      }

      Console.ReadKey();

    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SvgToTrack
{
  public class SvgCommand
  {
    public char command { get; private set; }
    public float[] arguments { get; private set; }

    public SvgCommand(char command, params float[] arguments)
    {
      this.command = command;
      this.arguments = arguments;
    }

    public static SvgCommand Parse(string SVGpathstring)
    {
      var cmd = SVGpathstring.Take(1).Single();
      string remainingargs = SVGpathstring.Substring(1);

      string argSeparators = @"[\s,]|(?=-)";
      var splitArgs = Regex
          .Split(remainingargs, argSeparators)
          .Where(t => !string.IsNullOrEmpty(t));

      float[] floatArgs = splitArgs.Select(arg => float.Parse(arg, _enCulture)).ToArray();
      return new SvgCommand(cmd, floatArgs);
    }

    private static CultureInfo _enCulture = CultureInfo.GetCultureInfo("en-US");
  }
}
